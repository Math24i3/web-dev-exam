<?php


namespace app\core;


class ViewHandler
{
    public string $title = '';

    /**
     *
     * @param string $view
     * @param array $params
     * @return array|false|string|string[]
     */
    public function renderView(string $view, array $params = [])
    {
        $view = $this->getViewOnly($view, $params);
        $layout = $this->layoutContent();
        return str_replace('{{content}}', $view, $layout);
    }

    /**
     * @param $view
     */
    public function renderContent(string $content)
    {
        $layout = $this->layoutContent();
        return str_replace('{{content}}', $content, $layout);
    }

    /**
     * Handles the layout
     */
    public function layoutContent()
    {
        $layout = Application::$app->layout;
        // we check whether the controller is initialized and set it likewise
        if (isset(Application::$app->controller)) {
            $layout = Application::$app->controller->layout;
        }
        // Caching the output
        ob_start();

        include_once Application::$ROOT_DIR."/resources/views/layouts/$layout.php";
        // clearing the buffer
        return ob_get_clean();
    }

    /**
     * @param string $view
     * @param array $params
     * @return false|string
     */
    public function getViewOnly(string $view, array $params)
    {
        foreach ($params as $key => $param) {
            // $$ evaluates = means that the key name is the same aas the param value
            $$key = $param;
        }

        // Caching the output - output is stored in an internal buffer.
        ob_start();
        include_once Application::$ROOT_DIR."/resources/views/$view.php";
        // Get current buffer contents and delete current output buffer
        return ob_get_clean();
    }

}