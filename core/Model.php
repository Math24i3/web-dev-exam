<?php

namespace app\core;

abstract class Model
{
    public static array $properties = [];


    public function loadData(array $dataArray): Model
    {
        foreach ($dataArray as $key => $value) {
            // Checking whether the property of the dataArray exists in the model
            if (property_exists($this, $key)) {
                //Whatever key that matches a property name in the model, assigning the properties to the model
                $this->{$key} = $value;
            }
        }
        return $this;

    }

    public function renderData()
    {
        
    }

}