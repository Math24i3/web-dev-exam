<?php

namespace app\core;

/**
 *
 */
abstract class ORMModel extends Model
{
    /**
     * Returns a string with the table name corresponding to the model
     * @return string
     */
    abstract public function tableName(): string;

    /**
     * Returns an array all the attributes that corresponds to the model
     * and should be inserted in the database
     * @return array
     */
    abstract public function attributes(): array;

    /**
     *
     */
    public function save() {
        $table = $this->tableName();
        $attributes = $this->attributes();
        $params = array_map(fn($attr) => ":$attr", $attributes);

        // preparing the statement by inserting the table and implode the array of attributes to a string with comma
        $statement = $this->prepare("INSERT INTO $table(". implode(',', $attributes).") 
                                            VALUES (". implode(',', $params).")");

        foreach ($attributes as $attribute) {
            $statement->bindValue(":$attribute", $this->{$attribute});
        }

        return $statement->execute();
    }

    public function prepare($sqlQuery) {
        return Application::$app->dbConn->pdo->prepare($sqlQuery);
    }
}