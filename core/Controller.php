<?php

namespace app\core;

use app\core\Application;
use app\core\middlewares\BaseMiddleware;
use app\middleware\AuthMiddleware;

/**
 *
 */
class Controller
{

    /**
     * @var string
     */
    public string $layout = 'main';


    /**
     * the current action / method
     * @var string
     */
    public string $action = '';


    /**
     * @var BaseMiddleware[]
     */
    protected array $middlewares = [];

    /**
     * @param string $layout
     */
    public function setLayout(string $layout)
    {
        $this->layout = $layout;
    }

    /**
     * @param string $view
     * @param array $params
     * @return array|false|string|string[]
     */
    public function render(string $view, array $params = []) {
        return Application::$app->viewHandler->renderView($view, $params);
    }

    /**
     * @param BaseMiddleware $middleware
     */
    protected function applyMiddleware(BaseMiddleware $middleware)
    {
        $this->middlewares[] = $middleware;

    }

    /**
     * @return BaseMiddleware[]
     */
    public function getMiddlewares(): array
    {
        return $this->middlewares;
    }

    /**
     * @param BaseMiddleware[] $middlewares
     */
    public function setMiddlewares(array $middlewares): void
    {
        $this->middlewares = $middlewares;
    }





}