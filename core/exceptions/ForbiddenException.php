<?php


namespace app\core\exceptions;


class ForbiddenException extends \Exception
{

    protected $message = "You don't have permission to see this.";
    protected $code = 403;
}