<?php

namespace app\core;

use app\core\Validation\RequestValidation;

class Request extends RequestValidation
{

    /**
     * @return false|mixed|string
     */
    public function getPath()
    {
        $path = $_SERVER['REQUEST_URI'] ?? '/';
        $position = strpos($path, '?');

        if ($position !== false) {
            $path = substr($path, 0, $position);
        }

        return $path;
    }

    /**
     * @return string
     */
    public function whichMethod(): string
    {
        return strtolower($_SERVER['REQUEST_METHOD']);
    }

    /**
     * @return string
     */
    public function isGet(): string
    {
        return $this->whichMethod() === 'get';
    }

    /**
     * @return string
     */
    public function isPost(): string
    {
        return $this->whichMethod() === 'post';
    }

    /**
     * @return string
     */
    public function isPut(): string
    {
        return $this->whichMethod() === 'put';
    }

    /**
     * @return string
     */
    public function isDelete(): string
    {
        return $this->whichMethod() === 'delete';
    }

    /**
     * Get the body of the request
     * This method will protect the application from getting infected, by sanitizing the request
     * Filter out special characters
     * @return array
     * @throws \JsonException
     */
    public function getBody(): array
    {

        $body = [];
        if ($this->isGet()) {
            // Super global GET and check the value for special characters to be remoted = secure body data
            foreach ($_GET as $key => $value) {
                $body[$key] = filter_input(INPUT_GET, $key, FILTER_SANITIZE_SPECIAL_CHARS);
            }
        }
        if ($this->isPost() || $this->isPut() || $this->isDelete()) {
            foreach ($_POST as $key => $value) {
                $body[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);
            }
            if (empty($body)) {
                $inputJSON = file_get_contents('php://input');
                if ($inputJSON) {

                    $body = json_decode($inputJSON, TRUE, 512, JSON_THROW_ON_ERROR);
                }
            }
        }

        // if the data send is not form-data, we assume that the data comes in a raw format

        unset($body['url']);



        return $body;
    }












}