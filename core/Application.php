<?php

namespace app\core;

use app\core\Controller;
use app\models\Admin;
use app\models\Customer;

class Application
{

    public static string $ROOT_DIR;
    public string $layout = 'main';
    public Router $router;
    public Request $request;
    public Response $response;
    public AppSession $session;
    public DatabaseConnection $dbConn;
    public Controller $controller;
    public ViewHandler $viewHandler;

    public ?Customer $customer;
    public ?Admin $admin;

    public static Application $app;

    /**
     * @param $root
     * @param array $config
     */
    public function __construct($root, array $config)
    {
        self::$ROOT_DIR = $root;
        self::$app = $this;
        $this->request = new Request();
        $this->response = new Response();
        $this->session = new AppSession();
        $this->router = new Router($this->request, $this->response);
        $this->viewHandler = new ViewHandler();

        try {
            $this->dbConn = new DatabaseConnection($config['db']);
        } catch (\PDOException $e) {
            if (is_int($e->getCode())) {

                $this->response->setStatusCode(500);
            }
            echo $this->viewHandler->renderView('http/_error', [
                'exception' => $e
            ]);
        }


        if ($this->session->get('customer')) {
            $this->customer = (Customer::find($this->session->get('customer')));
        } elseif ($this->session->get('admin')) {
            $this->admin = new Admin();
        } else {
            $this->customer = null;
            $this->admin = null;
        }
    }

    /**
     * checking if any user is logged in
     * @return bool
     */
    public static function isGuest(): bool
    {
        if (isset(self::$app->customer)) {
            return false;
        }

        if (isset(self::$app->admin)) {
            return false;
        }
        return true;
    }

    /**
     * checking if any user is logged in
     * @param string $user
     * @return bool
     */
    public static function is(string $user): bool
    {
        if (isset(self::$app->{$user})) {
            return true;
        }

        return false;
    }

    /**
     * @return void
     */
    public function run(): void
    {
        try {
            echo $this->router->resolve();
        } catch (\Exception $e) {
            if (is_int($e->getCode())) {

                $this->response->setStatusCode($e->getCode());
            }
            echo $this->viewHandler->renderView('http/_error', [
                'exception' => $e
            ]);
        }
    }

    public function customerLogin(Customer $customer): bool
    {
        $this->customer = $customer;
        $primaryKey = $customer->getCustomerId();
        $this->session->set('customer', $primaryKey);
        return true;
    }

    public function adminLogin(Admin $admin) {
        $this->admin = $admin;
        $this->session->set('admin', 1);
        return true;
    }

    public function customerLogout(): void
    {
        $this->customer = null;
        $this->session->remove('customer');
        $this->session->remove('cart');
    }

    public function adminLogout(): void
    {
        $this->admin = null;
        $this->session->remove('admin');
    }


}