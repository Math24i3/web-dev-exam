<?php


namespace app\core;


/**
 * Class AppSession
 * @package app\core
 */
class AppSession
{
    /**
     *
     */
    protected const SESSION_MESSAGE_KEY = 'session_messages';

    /**
     * AppSession constructor.
     */
    public function __construct()
    {
        session_start();
        $sessionMessages = $_SESSION[self::SESSION_MESSAGE_KEY] ?? [];
        // In some cases we only want the message to be shown at a certain request. To allow that we need to remove it
        // when usein the & we specify a reference to the original value
        foreach ($sessionMessages as $key => &$sessionMessage) {
            $sessionMessage['remove'] = true;
        }
        $_SESSION[self::SESSION_MESSAGE_KEY] = $sessionMessages;
        unset($sessionMessage);
    }

    /**
     * To set the message
     * @param $key
     * @param $message
     */
    public function setSessionMessage($key, $message): void
    {
        $_SESSION[self::SESSION_MESSAGE_KEY][$key] = [
            'remove' => false,
            'value' => $message
        ];
    }

    /**
     * To get the message
     * @param $key
     * @return false|mixed
     */
    public function getSessionMessage($key)
    {
        return $_SESSION[self::SESSION_MESSAGE_KEY][$key]['value'] ?? false;
    }

    /**
     * set a session value
     * @param $key
     * @param $value
     *
     */
    public function set($key, $value): void
    {
        $_SESSION[$key] = $value;
    }

    /**
     * appends a value to a session value
     * @param $key
     * @param $value
     *
     */
    public function append($key, $value, $identifier): void
    {
        $_SESSION[$key][$identifier] = $value;
    }

    /**
     * appends a value to a session value
     * @param $key
     * @param $identifier
     * @param $value
     */
    public function remove($key): void
    {
        unset($_SESSION[$key]);
    }

    /**
     * appends a value to a session value
     * @param $key
     * @param $identifier
     * @param $value
     */
    public function unset($key, $identifier): void
    {
        foreach ($_SESSION[$key] as $k => $val) {
            if ($k === (int)$identifier) {
                unset($_SESSION[$key][$k]);
            }
        }
    }

    /**
     * appends a value to a session value
     * @param $key
     * @param $value
     *
     */
    public function empty($key): void
    {
        unset($_SESSION[$key]);
    }

    /**
     * get a session value
     * @param $key
     * @return false|mixed
     */
    public function get($key)
    {
        // return $_SESSION[$key] ?? false;
        return $_SESSION[$key] ?? false;
    }

    /**
     * Destructor for removing the messages when the session dies
     */
    public function __destruct()
    {
        $this->removeSessionMessages();
    }

    /**
     * removes the session messages
     */
    private function removeSessionMessages(): void
    {
        $sessionMessages = $_SESSION[self::SESSION_MESSAGE_KEY] ?? [];
        foreach ($sessionMessages as $key => $sessionMessage) {
            if ($sessionMessage['remove']) {
                unset($sessionMessages[$key]);
            }
        }
        $_SESSION[self::SESSION_MESSAGE_KEY] = $sessionMessages;
    }

}