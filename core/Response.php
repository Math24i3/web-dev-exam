<?php

namespace app\core;

/**
 * Class Response
 * @package app\core
 */
class Response
{

    /**
     *  Returns the http response code
     * @param int $statusCode
     * @return int
     */
    public function setStatusCode(int $statusCode): int
    {
        return http_response_code($statusCode);
    }

    /**
     * Sets the location of a redirection
     * Send a raw HTTP header
     * @param string $url
     */
    public function redirect(string $url)
    {
        return header('Location: ' . $url);
    }

}