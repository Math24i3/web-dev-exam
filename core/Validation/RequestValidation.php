<?php

namespace app\core\Validation;

/**
 * Validation Class ofr request validation of any kind
 */
abstract class RequestValidation
{

    protected array $validationErrors = [];

    /**
     * @return array
     */
    public function getValidationErrors(): array
    {
        return $this->validationErrors;
    }

    /**
     * @param array $validationErrors
     */
    public function setValidationErrors(array $validationErrors): void
    {
        $this->validationErrors = $validationErrors;
    }


    /**
     * Validate the request by: required, confirm, min, max, int, string, email
     * @param array $validationRules
     * @return array|bool
     */
    public function validate(array $validationRules = []) {
        $validationErrors = [];
        if (isset($validationRules)) {
            foreach ($validationRules as $key => $value) {
                $rules = explode("|", $value);
                $dataValue = $this->getBody()[$key] ?? null;

                foreach ($rules as $rule) {
                    // If required
                    if (($rule === 'required') && !$dataValue) {
                       // $validationErrors[$key] ?? $validationErrors[$key] = [];
                        $validationErrors[$key][] = "$key is missing.";
                    }
                    if (isset($dataValue)) {

                        // min length
                        if (str_contains($rule, 'min')) {
                            $minRule = explode(":", $rule);
                            if (strlen($dataValue) < $minRule[1]) {
                                //$validationErrors[$key] ?? $validationErrors[$key] = [];
                                $validationErrors[$key][] = "Must be larger than $minRule[1] characters.";
                            }
                        }

                        // max length
                        if (str_contains($rule, 'max')) {
                            $minRule = explode(":", $rule);
                            if (strlen($dataValue) > $minRule[1]) {
                                //$validationErrors[$key] ?? $validationErrors[$key] = [];
                                $validationErrors[$key][] = "Must be less than $minRule[1] characters.";
                            }
                        }
                        // confirm password
                        if ($rule === 'confirm') {
                            if (isset($this->getBody()['confirm'])) {
                                if ($dataValue !== $this->getBody()['confirm']) {
                                    //$validationErrors[$key] ?? $validationErrors[$key] = [];
                                    $validationErrors[$key][] = "Password does not match.";
                                }
                            } else {
                                $validationErrors[$key][] = "Confirm field is missing.";
                            }
                        }
                        // email
                        if (($rule === 'email') && !filter_var($dataValue, FILTER_VALIDATE_EMAIL)) {
                            //$validationErrors[$key] ?? $validationErrors[$key] = [];
                            $validationErrors[$key][] = "Must be an email.";
                        }
                        // int
                        if (($rule === 'int') && !is_int($dataValue) && !(int)$dataValue) {
                            $validationErrors[$key][] = "Must be an integer.";
                        }
                        // string
                        if (($rule === 'string') && !is_string($dataValue)) {
                            //$validationErrors[$key] ?? $validationErrors[$key] = [];
                            $validationErrors[$key][] = "Must be a string.";
                        }

                        // float
                        if (($rule === 'float') && !is_float($dataValue)) {
                            $validationErrors[$key][] = "Must be a float.";
                        }

                        // boolean
                        if ($rule === 'bool') {
                            if (!is_bool(json_decode($dataValue))) {
                                //$validationErrors[$key] ?? $validationErrors[$key] = [];
                                $validationErrors[$key][] = "Must be a boolean.";
                            }
                        }
                    }
                }
            }
        }
        if (!empty($validationErrors)) {
            $this->setValidationErrors($validationErrors);
            return false;
        }

        return true;
    }

}