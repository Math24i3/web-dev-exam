<?php

namespace app\core;
use app\core\exceptions\NotFoundException;
use Closure;

/**
 *
 */
class Router
{
    /**
     * @var Request
     */
    public Request $request;

    /**
     * @var Response
     */
    public Response $response;

    /**
     * @var array
     */
    private array $routes = [];


    /**
     * @param Request $request
     * @param Response $response
     */
    public function __construct(Request $request, Response $response)
    {
        $this->request = $request;
        $this->response = $response;
    }


    /**
     * GET Method for Routes
     * @param string $path
     * @param $callback
     */
    public function get(string $path, $callback): void
    {
        $this->routes['get'][$path] = $callback;
    }

    /**
     * @param string $path
     * @param $callback
     */
    public function post(string $path, $callback): void
    {
        $this->routes['post'][$path] = $callback;
    }

    /**
     * @param string $path
     * @param $callback
     */
    public function put(string $path, $callback): void
    {
        $this->routes['put'][$path] = $callback;
    }

    /**
     * @param string $path
     * @param $callback
     */
    public function delete(string $path, $callback): void
    {
        $this->routes['delete'][$path] = $callback;
    }

    /**
     * Resolves the route / request
     * @throws NotFoundException
     */
    public function resolve()
    {

        $method = $this->request->whichMethod();
        $path = $this->request->getPath();

        $callback = $this->routes[$method][$path] ?? false;
        if (!$callback) {
            throw new NotFoundException();
        }
        if (is_string($callback)) {
            return Application::$app->viewHandler->renderView($callback);
        }

        if (is_array($callback)) {
            /**
             * Important step because we a checking for middleware, before returning the callback
             * @var $controller Controller
             */
            $controller = new $callback[0];
            $controller->action = $callback[1];
            Application::$app->controller = $controller;
            $middlewares = $controller->getMiddlewares();
            foreach ($middlewares as $middleware) {
                $middleware->exec();
            }
            $callback[0] = $controller;
        }

        return $callback($this->request, $this->response);
    }
}