<?php

//Routes
use app\controllers\AdminController;
use app\controllers\AuthController;
use app\controllers\CartController;
use app\controllers\CustomerController;
use app\controllers\EditController;
use app\controllers\HomeController;


/** @var $app \app\core\Application */


$app->router->get('/', [HomeController::class, 'home']);

//Login
$app->router->get('/login', [AuthController::class, 'login']);
$app->router->post('/login', [AuthController::class, 'login']);

$app->router->get('/adminlogin', [AdminController::class, 'login']);
$app->router->post('/adminlogin', [AdminController::class, 'login']);

//Logout
$app->router->get('/logout', [AuthController::class, 'logout']);

//Register
$app->router->get('/register', [AuthController::class, 'register']);
$app->router->post('/register', [AuthController::class, 'register']);

//Profile
$app->router->get('/profile', [CustomerController::class, 'profile']);

//Browse
$app->router->get('/browse', [HomeController::class, 'browse']);

//Cart
$app->router->get('/cart', [HomeController::class, 'cart']);

//Checkout
$app->router->get('/checkout', [CartController::class, 'checkout']);
$app->router->post('/checkout', [CartController::class, 'checkout']);

//Edit
$app->router->get('/edit', [AdminController::class, 'editContent']);
