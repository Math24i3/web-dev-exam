<?php

//Routes
use app\controllers\AlbumController;
use app\controllers\ArtistController;
use app\controllers\AuthController;
use app\controllers\CartController;
use app\controllers\CustomerController;
use app\controllers\GenreController;
use app\controllers\HomeController;
use app\controllers\MediaTypeController;
use app\controllers\TrackController;


/** @var $app \app\core\Application */

//Customer
$app->router->get('/api/customer',          [CustomerController::class, 'get']);
$app->router->put('/api/customer',          [CustomerController::class, 'put']);
$app->router->put('/api/customer/password', [CustomerController::class, 'updatePassword']);

//Track
$app->router->get('/api/track',     [TrackController::class, 'get']);
$app->router->put('/api/track',     [TrackController::class, 'put']);
$app->router->post('/api/track',    [TrackController::class, 'post']);
$app->router->delete('/api/track',  [TrackController::class, 'delete']);
$app->router->get('/api/tracks',    [TrackController::class, 'all']);

//Artist
$app->router->get('/api/artist',    [ArtistController::class, 'get']);
$app->router->put('/api/artist',    [ArtistController::class, 'put']);
$app->router->post('/api/artist',   [ArtistController::class, 'post']);
$app->router->delete('/api/artist', [ArtistController::class, 'delete']);
$app->router->get('/api/artists',   [ArtistController::class, 'all']);

//Album
$app->router->get('/api/album',     [AlbumController::class, 'get']);
$app->router->put('/api/album',     [AlbumController::class, 'put']);
$app->router->post('/api/album',    [AlbumController::class, 'post']);
$app->router->delete('/api/album',  [AlbumController::class, 'delete']);
$app->router->get('/api/albums',    [AlbumController::class, 'all']);

//Cart
$app->router->get('/api/cart',          [CartController::class, 'get']);
$app->router->put('/api/cart',          [CartController::class, 'put']);
$app->router->delete('/api/cart',       [CartController::class, 'delete']);
$app->router->delete('/api/cart/empty', [CartController::class, 'deleteAll']);

//MediaTypes
$app->router->get('/api/media-types', [MediaTypeController::class, 'all']);

//Genre
$app->router->get('/api/genres', [GenreController::class, 'all']);