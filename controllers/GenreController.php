<?php


namespace app\controllers;


use app\core\Controller;
use app\core\Request;
use app\core\Response;
use app\middleware\AuthMiddleware;
use app\models\Genre;

class GenreController extends Controller
{
    public function __construct()
    {
        $this->applyMiddleware(new AuthMiddleware(['all'], 'admin'));
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return false|string
     * @throws \JsonException
     */
    public function all(Request $request, Response $response)
    {

        $genres = Genre::all();

        if (!$genres) {
            $responseData['errors'] = ['genres not found'];
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_THROW_ON_ERROR | JSON_INVALID_UTF8_IGNORE);
        }
        $response->setStatusCode(200);
        return json_encode($genres, JSON_THROW_ON_ERROR | JSON_INVALID_UTF8_IGNORE);
    }

}