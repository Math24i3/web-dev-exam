<?php


namespace app\controllers;


use app\core\Application;
use app\core\Controller;
use app\core\Request;
use app\core\Response;
use app\middleware\AuthMiddleware;
use app\models\Customer;
use app\models\Track;
use app\repositories\InvoiceRepository;

/**
 * Class CartController
 * @package app\controllers
 */
class CartController extends Controller
{

    public function __construct()
    {
        $this->applyMiddleware(new AuthMiddleware(['put', 'post', 'delete', 'deleteAll', 'checkout'], 'customer'));
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return false|string
     * @throws \JsonException
     */
    public function get(Request $request, Response $response) {
        try {
            $cart = Application::$app->session->get('cart');

        } catch (\Exception $e) {
            $responseData['errors'][] = $e->getMessage();
            $response->setStatusCode(500);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }
        if (!$cart) {
            $responseData['message'][] = 'Cart not found or Empty';
            $response->setStatusCode(200);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }

        $response->setStatusCode(200);
        $responseData['cart'] = $cart;
        return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);

    }

    /**
     * @param Request $request
     * @param Response $response
     * @return false|string
     * @throws \JsonException
     */
    public function put(Request $request, Response $response)
    {
        $validation = $request->validate([
            'trackId'      => 'int|required',

        ]);

        if (!$validation ) {
            $responseData['errors'] = $request->getValidationErrors();
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }
        $reqBody = $request->getBody();

        $track = Track::find($reqBody['trackId']);
        if (!$track) {
            $responseData['errors'][] = 'Track not found';
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }


        Application::$app->session->append('cart', $track, $track->getTrackId());
        $response->setStatusCode(200);
        $data = [
            'message'   => 'Track was added to cart',
            'track'     => $track,
            'cart'      => Application::$app->session->get('cart')
        ];
        return json_encode($data, JSON_INVALID_UTF8_IGNORE);

    }

    /**
     * @param Request $request
     * @param Response $response
     * @return false|string
     * @throws \JsonException
     */
    public function delete(Request $request, Response $response)
    {
        $reqBody = $request->getBody();
        $validation = $request->validate([
            'trackId'      => 'int|required',

        ]);

        if (!$validation ) {
            $responseData['errors'] = $request->getValidationErrors();
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }

        $track = Track::find($reqBody['trackId']);
        if (!$track) {
            $responseData['errors'][] = 'Track not found';
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }


        Application::$app->session->unset('cart', $track->getTrackId());
        $response->setStatusCode(200);
        $data = [
            'message'   => 'Track was removed from cart',
            'track'     => $track,
            'cart'      => Application::$app->session->get('cart')
        ];
        return json_encode($data, JSON_INVALID_UTF8_IGNORE);

    }

    /**
     * @param Request $request
     * @param Response $response
     * @return false|string
     * @throws \JsonException
     */
    public function deleteAll(Request $request, Response $response)
    {

        Application::$app->session->empty('cart');
        $response->setStatusCode(200);
        $data = [
            'message'   => 'Cart is empty',
            'cart'      => Application::$app->session->get('cart')
        ];
        return json_encode($data, JSON_INVALID_UTF8_IGNORE);

    }

    /**
     * @param Request $request
     * @param Response $response
     * @return array|false|string|string[]|void
     * @throws \JsonException
     */
    public function checkout(Request $request, Response $response) {
        $cart = Application::$app->session->get('cart');
        if (!$cart) {
            Application::$app->session->setSessionMessage('error', 'Cart is empty!');
            return $response->redirect('/cart');
        }

        $totalPrice = 0;
        foreach ($cart as $track) {
            $totalPrice += $track->getUnitPrice();
        }

        $customerId = Application::$app->session->get('customer');

        $customer = Customer::find($customerId);

        // If request is post
        if ($request->isPost()) {
            $responseData['totalPrice'] = $totalPrice;

            if (!$customer) {
                return $this->render('login');
            }
            $responseData['customer'] = $customer;


            $validation = $request->validate([
                'firstName' => 'string',
                'lastName' => 'string',
                'email' => 'string|email',
                'address' => 'string|required',
                'city' => 'string|required',
                'state' => 'string|required',
                'country'=> 'string|required',
                'postalCode' => 'string|required',
                'token'     => 'required'
            ]);

            $reqBody = $request->getBody();
            if (!$validation ) {
                $responseData['errors'] = $request->getValidationErrors();
                return $this->render('checkout', $responseData);
            }

            $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

            if ($token !== $_SESSION['token']) {
                $response->setStatusCode(405);
                $responseData = [
                    'errors' => ['token' => 'Invalid form submission']
                ];
                return $this->render('checkout', $responseData);
            }


            $data = [
                'CustomerId' => (int)$customerId,
                'InvoiceDate' => date('Y-m-d H:i:s'),
                'BillingAddress' => $reqBody['address'],
                'BillingCity' => $reqBody['city'],
                'BillingState' => $reqBody['state'],
                'BillingCountry' => $reqBody['country'],
                'BillingPostalCode' => $reqBody['postalCode'],
                'Total' => round($totalPrice, 2),
                'Tracks' => $cart
            ];

            $res = InvoiceRepository::insertInvoice($data);
            if (!$res) {
                $response->setStatusCode(500);
                $responseData['errors'] = 'An error occurred. Try again';
                return $this->render('checkout', $responseData);
            }
            Application::$app->session->empty('cart');
            Application::$app->session->setSessionMessage('success', 'Purchase was made!');
            return $this->render('home', $responseData);
        }

        $data = [
            'cart'      => Application::$app->session->get('cart'),
            'totalPrice'=> $totalPrice,
            'customer'  => $customer ?? null
        ];
        return $this->render('checkout', $data);

    }

}