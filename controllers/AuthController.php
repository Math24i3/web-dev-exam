<?php

namespace app\controllers;

use app\core\Application;
use app\core\Controller;
use app\core\Request;
use app\core\Response;
use app\models\Customer;
use JsonException;

/**
 *
 */
class AuthController extends Controller
{


    /**
     * Login method
     * @param Request $request
     * @param Response $response
     * @return array|false|string|string[]
     * @throws JsonException
     */
    public function login(Request $request, Response $response) {
        if ($request->isPost()) {
            $this->setLayout('auth');


            $validation = $request->validate([
                'email'     => 'string|required|min:1|max:50|email',
                'password'  => 'string|required|min:2',
                'token'     => 'required'
            ]);
            if (!$validation ) {
                //return var_export($request->getValidationErrors(), true);
                $responseData = [
                    'errors' => $request->getValidationErrors()
                ];
                //$responseData['errors'] = $request->getValidationErrors();
                return $this->render('login', $responseData);
            }


            $data = $request->getBody();
            $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

            if ($token !== $_SESSION['token']) {
                $response->setStatusCode('405');
                $responseData = [
                    'errors' => ['token' => 'Invalid form submission']
                ];
                return $this->render('login', $responseData);
            }

            $customer = Customer::getByEmail($data['email']);

            if (!$customer) {
                Application::$app->session->setSessionMessage('error', 'failed');
                return $response->redirect('login');
            }
            if ($this->authenticate($data['password'], $customer->getPassword())) {
                Application::$app->customerLogout();
                Application::$app->adminLogout();
                Application::$app->customerLogin($customer);
                //Application::$app->session->setSessionMessage('success', 'user login!');
                return $response->redirect('/');
            }
            Application::$app->session->setSessionMessage('error', 'failed');
            return $response->redirect('login');

        }

        $this->setLayout('auth');
        return $this->render('login');
    }

    /**
     * @param $password
     * @param $comparePwd
     * @return bool
     */
    private function authenticate($password, $comparePwd): bool
    {
        return password_verify($password, $comparePwd);
    }

    /**
     * @param Request $request
     * @param Response $response
     */
    public function logout(Request $request, Response $response)
    {
        Application::$app->customerLogout();
        Application::$app->adminLogout();
        return $response->redirect('/');
    }

    /**
     * Register Model
     * @param Request $request
     * @param Response $response
     * @return array|false|string|string[]
     * @throws JsonException
     */
    public function register(Request $request, Response $response) {
        echo
        $this->setLayout('auth');
        if ($request->isPost()) {
            $responseData = [];
            $validation = $request->validate([
                'FirstName'     => 'string|required|min:1|max:12',
                'LastName'      => 'string|required|min:1|max:12',
                'Email'         => 'string|required|min:1|max:50|email',
                'Password'      => 'string|required|min:1|max:12|confirm',
                'token'         => 'required'
            ]);
            if (!$validation ) {
                //return var_export($request->getValidationErrors(), true);
                $responseData['errors'] = $request->getValidationErrors();
                return $this->render('register', $responseData);
            }

            $token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);

            if ($token !== $_SESSION['token']) {
                $response->setStatusCode('405');
                $responseData = [
                    'errors' => ['token' => 'Invalid form submission']
                ];
                return $this->render('register', $responseData);
            }

            $customer = new Customer();

            try {
                //Try to save the model in the database
                $reqBody = $request->getBody();
                unset($reqBody['confirm'], $reqBody['token']);
                $saveRes = $customer->save($reqBody);
            } catch (\PDOException $e) {
                // SQL error for unique error
                if ($e->getCode() === '23000') {
                    $responseData['errors']['Email'] = ["The email is already taken"];
                } else {
                    $responseData['errors'] = "An error occurred. Please try again or contact sys admin";
                }
                return $this->render('register', $responseData);
            }
            if (!$saveRes) {
                $responseData['errors'] = "An error occurred. Please try again or contact sys admin";
                return $this->render('register', $responseData);
            }

            $customer = Customer::getByEmail($reqBody['Email']);

            if (!$customer) {
                Application::$app->session->setSessionMessage('error', 'failed');
                return $response->redirect('login');
            }
            if ($this->authenticate($reqBody['Password'], $customer->getPassword())) {
                Application::$app->customerLogin($customer);
                return $response->redirect('/');
            }
            //Application::$app->session->setSessionMessage('success', 'register success!');

            return $response->redirect('/');
        }

        return $this->render('register');
    }


}