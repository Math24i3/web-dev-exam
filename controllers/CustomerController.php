<?php
namespace app\controllers;

use app\core\Application;
use app\core\Controller;
use app\core\Request;
use app\core\Response;
use app\middleware\AuthMiddleware;
use app\models\Customer;

/**
 * Class CustomerController
 * @package app\controllers
 */
class CustomerController extends Controller
{

    /**
     * constructor.
     */
    public function __construct()
    {
        $this->applyMiddleware(new AuthMiddleware(['get', 'put', 'updatePassword', 'profile'], 'customer'));
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return false|string
     * @throws \JsonException
     */
    public function get(Request $request, Response $response)
    {
        $reqBody = $request->getBody();
        $validation = $request->validate([
            'id'      => 'required'
        ]);
        if (!$validation ) {
            $responseData['errors'] = $request->getValidationErrors();
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }
        $customer = Customer::find($reqBody['id']);
        $response->setStatusCode(200);
        return json_encode($customer, JSON_INVALID_UTF8_IGNORE);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return false|string
     * @throws \JsonException
     */
    public function put(Request $request, Response $response)
    {
        $reqBody = $request->getBody();
        $validation = $request->validate([
            'CustomerId' => 'required|int',
            'FirstName' => 'string',
            'LastName' => 'string',
            'Company' => 'string',
            'Address' => 'string',
            'City' => 'string',
            'State' => 'string',
            'Country'=> 'string',
            'PostalCode' => 'string',
            'Phone' => 'string',
            'Fax' => 'string',
            'Email' => 'string|email'
        ]);

        if (!$validation ) {
            $responseData['errors'] = $request->getValidationErrors();
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }
        $customer = Customer::find($reqBody['CustomerId']);
        if (!$customer) {
            $responseData['errors'] = 'Customer was not found.';
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }
        try {
            $update = Customer::update($reqBody['CustomerId'], $reqBody);

        } catch (\PDOException $e) {
            // SQL error for unique error
            if ($e->getCode() === '23000') {
                $responseData['errors']['Email'] = ["The email is already taken"];
            } else {
                $responseData['errors'] = "An error occurred. Please try again or contact sys admin";
            }
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }
        if (!$update) {
            $responseData['errors'] = ['Customer was not updated.'];
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }

        if (isset($update['status']) && $update['status'] === false) {
            $responseData['errors'] = $update['error'];
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }
        $responseData['success'] = ['Customer was updated!.'];
        return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return false|string
     * @throws \JsonException
     */
    public function updatePassword(Request $request, Response $response)
    {
        $reqBody = $request->getBody();
        $validation = $request->validate([
            'CustomerId' => 'required|int',
            'Old' => 'string|required',
            'Password' => 'string|required|confirm',
        ]);

        if (!$validation ) {
            $responseData['errors'] = $request->getValidationErrors();
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }
        $customer = Customer::find($reqBody['CustomerId']);
        if (!$customer) {
            $responseData['errors'] = ['Customer was not found.'];
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }

        if (!password_verify($reqBody['Old'], $customer->getPassword())) {
            $responseData['errors'] = ['Old password was wrong.'];
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }
        $data = [
            'Password' => password_hash($reqBody['Password'], PASSWORD_DEFAULT)
        ];

        $update = Customer::update($reqBody['CustomerId'], $data);
        if (!$update) {
            $responseData['errors'] = ['Password was not updated.'];
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }
        $responseData['success'] = 'Password was updated!.';
        return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
    }

    /**
     * Profile
     * @param Request $request
     * @param Response $response
     * @return array|false|string|string[]
     */
    public function profile(Request $request, Response $response) {
        $customerId = Application::$app->customer->getCustomerId();
        if (!$customerId) {
            Application::$app->session->setSessionMessage('error', 'Not found!');
            return $response->redirect('/');
        }
        $customer = Customer::find($customerId);
        if (!$customer) {
            Application::$app->session->setSessionMessage('error', 'Not found!');
            return $response->redirect('/');
        }

        return $this->render('profile', [
            'customer' => $customer
        ]);
    }

}