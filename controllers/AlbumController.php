<?php


namespace app\controllers;


use app\core\Controller;
use app\core\Request;
use app\core\Response;
use app\middleware\AuthMiddleware;
use app\models\Album;
use app\models\Artist;
use app\models\Track;
use app\repositories\AlbumRepository;
use app\repositories\TrackRepository;

/**
 * Class AlbumController
 * @package app\controllers
 */
class AlbumController extends Controller
{

    public function __construct()
    {
        $this->applyMiddleware(new AuthMiddleware(['put', 'post', 'delete'], 'admin'));
        $this->applyMiddleware(new AuthMiddleware(['get', 'all']));
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return false|string
     * @throws \JsonException
     */
    public function get(Request $request, Response $response)
    {
        $reqBody = $request->getBody();
        $validation = $request->validate([
            'id'      => 'required'
        ]);
        if (!$validation ) {
            $responseData['errors'] = $request->getValidationErrors();
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }
        $album = Album::find($reqBody['id']);
        $response->setStatusCode(200);
        return json_encode($album, JSON_INVALID_UTF8_IGNORE);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return false|string
     * @throws \JsonException
     */
    public function put(Request $request, Response $response)
    {

        $validation = $request->validate([
            'AlbumId'       => 'required|int',
            'Title'          => 'string',
        ]);
        if (!$validation ) {
            $responseData['errors'] = $request->getValidationErrors();
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }
        $reqBody = $request->getBody();

        $album = Album::find($reqBody['AlbumId']);
        if (!$album) {
            $responseData['errors'] = 'Album was not found.';
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }
        try {
            $update = Album::update($reqBody['AlbumId'], $reqBody);

        } catch (\PDOException $e) {
            $responseData['errors'] = "An error occurred. Please try again or contact sys admin";
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }
        if (!$update) {
            $responseData['errors'] = ['Album was not updated.'];
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }

        if (isset($update['status']) && $update['status'] === false) {
            $responseData['errors'] = $update['error'];
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }
        $responseData['success'] = ['Album was updated!.'];
        return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return false|string
     * @throws \JsonException
     */
    public function post(Request $request, Response $response)
    {
        $validation = $request->validate([
            'Title'          => 'string|required',
            'ArtistId'       => 'int`|required'
        ]);
        if (!$validation ) {
            $responseData['errors'] = $request->getValidationErrors();
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }

        $album = new Album();

        try {
            //Try to save the model in the database
            $reqBody = $request->getBody();
            $saveRes = $album->save($reqBody);
        } catch (\PDOException $e) {
            $responseData['errors'] = "An error occurred. Please try again or contact sys admin";

            return json_encode($responseData, JSON_THROW_ON_ERROR | JSON_INVALID_UTF8_IGNORE);
        }
        if (!$saveRes) {
            $responseData['errors'] = "An error occurred. Please try again or contact sys admin";
            return json_encode($responseData, JSON_THROW_ON_ERROR | JSON_INVALID_UTF8_IGNORE);
        }

        $response->setStatusCode(200);
        return json_encode($saveRes, JSON_THROW_ON_ERROR | JSON_INVALID_UTF8_IGNORE);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return false|string
     * @throws \JsonException
     */
    public function delete(Request $request, Response $response)
    {

        $validation = $request->validate([
            'AlbumId'      => 'required'
        ]);
        if (!$validation ) {
            $responseData['errors'] = $request->getValidationErrors();
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }
        $reqBody = $request->getBody();
        $deleteRes = Album::delete($reqBody['AlbumId']);
        if (!$deleteRes) {
            $responseData['errors'] = ['Album was not deleted'];
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_THROW_ON_ERROR | JSON_INVALID_UTF8_IGNORE);
        }

        if ($deleteRes === '23000') {
            $responseData['errors'] = ['Album cannot be deleted due to relation'];
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_THROW_ON_ERROR | JSON_INVALID_UTF8_IGNORE);
        }
        $response->setStatusCode(200);
        return json_encode('Album was deleted', JSON_THROW_ON_ERROR | JSON_INVALID_UTF8_IGNORE);
    }


    /**
     * @param Request $request
     * @param Response $response
     * @return false|string
     * @throws \JsonException
     */
    public function all(Request $request, Response $response)
    {
        $reqBody = $request->getBody();
        $validation = $request->validate([
            'withRelations'      => 'bool'
        ]);

        if (!$validation ) {
            $responseData['errors'] = $request->getValidationErrors();
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }
        if (isset($reqBody['withRelations']) && json_decode($reqBody['withRelations']) === true) {
            $albums = AlbumRepository::allWithRelations($reqBody['perPage'] ?? 25, $reqBody['page'] ?? 1);
            $response->setStatusCode(200);
            return json_encode($albums, JSON_INVALID_UTF8_IGNORE);
        }

        if (isset($reqBody['withRelations']) && json_decode($reqBody['withRelations']) === false) {
            $albums = Album::all();
            $response->setStatusCode(200);
            return json_encode($albums, JSON_INVALID_UTF8_IGNORE);
        }

        $albums = Album::all();
        $response->setStatusCode(200);
        return json_encode($albums, JSON_INVALID_UTF8_IGNORE);
    }

}