<?php


namespace app\controllers;


use app\core\Application;
use app\core\Controller;
use app\core\Request;
use app\core\Response;
use app\middleware\AuthMiddleware;
use app\models\Admin;
use app\models\Customer;
use JsonException;

/**
 * Class AdminController
 * @package app\controllers
 */
class AdminController extends Controller
{
    /**
     * AdminController constructor.
     */
    public function __construct()
    {
        $this->applyMiddleware(new AuthMiddleware(['editContent'], 'admin'));
    }


    /**
     * Login method
     * @param Request $request
     * @param Response $response
     * @return array|false|string|string[]
     * @throws JsonException
     */
    public function login(Request $request, Response $response) {
        if ($request->isPost()) {
            $this->setLayout('auth');

            $validation = $request->validate([
                'password'  => 'string|required|min:2'
            ]);

            if (!$validation ) {
                $responseData = [
                    'errors' => $request->getValidationErrors()
                ];
                return $this->render('adminlogin', $responseData);
            }

            $data = $request->getBody();
            $admin = Admin::getTheOnlyAdminInTheDatabaseByTheHashedPasswordWhichIsImpossibleToExStract();

            if (!$admin) {
                Application::$app->session->setSessionMessage('error', 'failed');
                return $response->redirect('adminlogin');
            }
            if ($this->authenticate($data['password'], $admin->getPassword())) {
                Application::$app->customerLogout();
                Application::$app->adminLogout();
                Application::$app->adminLogin($admin);
                return $response->redirect('/');
            }
            Application::$app->session->setSessionMessage('error', 'failed');
            return $response->redirect('adminlogin');

        }

        $this->setLayout('auth');
        return $this->render('adminlogin');
    }

    /**
     * @return array|false|string|string[]
     */
    public function editContent () {
        $data = [
            'crudEndpoint' => '/api/_model_',

        ];
        return $this->render('edit', $data);
    }

    /**
     * @param $password
     * @param $comparePwd
     * @return bool
     */
    private function authenticate($password, $comparePwd): bool
    {
        return password_verify($password, $comparePwd);
    }

}