<?php


namespace app\controllers;


use app\core\Controller;
use app\core\Request;
use app\core\Response;
use app\middleware\AuthMiddleware;
use app\models\Customer;
use app\models\Track;
use app\repositories\TrackRepository;

/**
 * Class TrackController
 * @package app\controllers
 */
class TrackController extends Controller
{


    public function __construct()
    {
        $this->applyMiddleware(new AuthMiddleware(['put', 'post', 'delete'], 'admin'));
        $this->applyMiddleware(new AuthMiddleware(['get', 'all']));
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return false|string
     * @throws \JsonException
     */
    public function get(Request $request, Response $response)
    {
        $reqBody = $request->getBody();

        $validation = $request->validate([
            'id'      => 'required'
        ]);
        if (!$validation ) {
            $responseData['errors'] = $request->getValidationErrors();
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }
        $track = Track::find($reqBody['id']);
        $response->setStatusCode(200);
        return json_encode($track, JSON_INVALID_UTF8_IGNORE);
    }


    /**
     * @param Request $request
     * @param Response $response
     * @return false|string
     * @throws \JsonException
     */
    public function put(Request $request, Response $response)
    {

        $validation = $request->validate([
            'TrackId'       => 'required|int',
            'Name'          => 'string',
            'Composer'      => 'string',
            'Milliseconds'  => 'int',
            'UnitPrice'     => 'float'
        ]);
        if (!$validation ) {
            $responseData['errors'] = $request->getValidationErrors();
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }
        $reqBody = $request->getBody();

        $track = Track::find($reqBody['TrackId']);
        if (!$track) {
            $responseData['errors'] = 'Track was not found.';
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }
        try {
            $update = Track::update($reqBody['TrackId'], $reqBody);

        } catch (\PDOException $e) {
            $responseData['errors'] = "An error occurred. Please try again or contact sys admin";
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }
        if (!$update) {
            $responseData['errors'] = ['Track was not updated.'];
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }

        if (isset($update['status']) && $update['status'] === false) {
            $responseData['errors'] = $update['error'];
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }
        $responseData['success'] = ['Track was updated!.'];
        return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return false|string
     * @throws \JsonException
     */
    public function post(Request $request, Response $response)
    {
        $validation = $request->validate([
            'Name'          => 'string|required',
            'AlbumId'       => 'int',
            'MediaTypeId'   => 'int|required',
            'GenreId'       => 'int',
            'Composer'      => 'string',
            'Milliseconds'   => 'int|required',
            'Bytes'   => 'int|required',
            'UnitPrice'     => 'float',

        ]);
        if (!$validation ) {
            $responseData['errors'] = $request->getValidationErrors();
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }

        $track = new Track();

        try {
            //Try to save the model in the database
            $reqBody = $request->getBody();
            $saveRes = $track->save($reqBody);
        } catch (\PDOException $e) {
            $responseData['errors'] = "An error occurred. Please try again or contact sys admin";

            return json_encode($responseData, JSON_THROW_ON_ERROR | JSON_INVALID_UTF8_IGNORE);
        }
        if (!$saveRes) {
            $responseData['errors'] = "An error occurred. Please try again or contact sys admin";
            return json_encode($responseData, JSON_THROW_ON_ERROR | JSON_INVALID_UTF8_IGNORE);
        }

        $response->setStatusCode(200);
        return json_encode($saveRes, JSON_THROW_ON_ERROR | JSON_INVALID_UTF8_IGNORE);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return false|string
     * @throws \JsonException
     */
    public function delete(Request $request, Response $response)
    {

        $validation = $request->validate([
            'TrackId'      => 'required'
        ]);
        if (!$validation ) {
            $responseData['errors'] = $request->getValidationErrors();
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }
        $reqBody = $request->getBody();
        $deleteRes = Track::delete($reqBody['TrackId']);
        if (!$deleteRes) {
            $responseData['errors'] = ['Track was not deleted'];
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_THROW_ON_ERROR | JSON_INVALID_UTF8_IGNORE);
        }

        if ($deleteRes === '23000') {
            $responseData['errors'] = ['Track cannot be deleted due to relation'];
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_THROW_ON_ERROR | JSON_INVALID_UTF8_IGNORE);
        }
        $response->setStatusCode(200);
        return json_encode('Track was deleted', JSON_THROW_ON_ERROR | JSON_INVALID_UTF8_IGNORE);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return false|string
     * @throws \JsonException
     */
    public function all(Request $request, Response $response)
    {
        $reqBody = $request->getBody();
        $validation = $request->validate([
            'withRelations'      => 'bool'
        ]);

        if (!$validation ) {
            $responseData['errors'] = $request->getValidationErrors();
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }
        if (isset($reqBody['withRelations']) && json_decode($reqBody['withRelations']) === true) {
            $tracks = TrackRepository::allWithRelations($reqBody['perPage'] ?? 25, $reqBody['page'] ?? 1);
            $response->setStatusCode(200);
            return json_encode($tracks, JSON_INVALID_UTF8_IGNORE);
        }

        if (isset($reqBody['withRelations']) && json_decode($reqBody['withRelations']) === false) {
            $tracks = Track::all();
            $response->setStatusCode(200);
            return json_encode($tracks, JSON_INVALID_UTF8_IGNORE);
        }

        $tracks = Track::all();
        $response->setStatusCode(200);
        return json_encode($tracks, JSON_INVALID_UTF8_IGNORE);
    }

}