<?php

namespace app\controllers;

use app\core\Application;
use app\core\Controller;
use app\core\Request;
use app\core\Response;
use app\middleware\AuthMiddleware;
use app\models\Customer;

/**
 *
 */
class HomeController extends Controller
{

    public function __construct()
    {
        $this->applyMiddleware(new AuthMiddleware(['browse', 'cart'], 'customer'));
    }

    /**
     * @return array|false|string|string[]
     */
    public function home()
    {
        $params = [
            'name' => 'Web dev'
        ];

        return $this->render('home', $params);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return array|false|string|string[]
     */
    public function browse(Request $request, Response $response)
    {
        $data = [
            'allTracksEndpoint' => '/tracks'
        ];

        return $this->render('browse', $data);
    }

    /**
     * @return array|false|string|string[]
     */
    public function cart() {

        return $this->render('cart');
    }

}