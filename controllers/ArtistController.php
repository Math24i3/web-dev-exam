<?php


namespace app\controllers;


use app\core\Controller;
use app\core\Request;
use app\core\Response;
use app\middleware\AuthMiddleware;
use app\models\Album;
use app\models\Artist;
use app\models\Track;
use app\repositories\AlbumRepository;
use app\repositories\ArtistRepository;

/**
 * Class ArtistController
 * @package app\controllers
 */
class ArtistController extends Controller
{

    public function __construct()
    {
        $this->applyMiddleware(new AuthMiddleware(['put', 'post', 'delete'], 'admin'));
        $this->applyMiddleware(new AuthMiddleware(['get', 'all']));
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return false|string
     * @throws \JsonException
     */
    public function get(Request $request, Response $response)
    {
        $reqBody = $request->getBody();
        $validation = $request->validate([
            'id'      => 'required'
        ]);
        if (!$validation ) {
            $responseData['errors'] = $request->getValidationErrors();
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }
        $artist = Artist::find($reqBody['id']);
        $response->setStatusCode(200);
        return json_encode($artist, JSON_INVALID_UTF8_IGNORE);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return false|string
     * @throws \JsonException
     */
    public function put(Request $request, Response $response)
    {

        $validation = $request->validate([
            'ArtistId'       => 'required|int',
            'Name'          => 'string',
        ]);
        if (!$validation ) {
            $responseData['errors'] = $request->getValidationErrors();
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }
        $reqBody = $request->getBody();

        $artist = Artist::find($reqBody['ArtistId']);
        if (!$artist) {
            $responseData['errors'] = 'Artist was not found.';
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }
        try {
            $update = Artist::update($reqBody['ArtistId'], $reqBody);

        } catch (\PDOException $e) {
            $responseData['errors'] = "An error occurred. Please try again or contact sys admin";
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }
        if (!$update) {
            $responseData['errors'] = ['Artist was not updated.'];
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }

        if (isset($update['status']) && $update['status'] === false) {
            $responseData['errors'] = $update['error'];
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }
        $responseData['success'] = ['Artist was updated!.'];
        return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return false|string
     * @throws \JsonException
     */
    public function post(Request $request, Response $response)
    {
        $validation = $request->validate([
            'Name'          => 'string|required',
        ]);
        if (!$validation ) {
            $responseData['errors'] = $request->getValidationErrors();
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }

        $artist = new Artist();

        try {
            //Try to save the model in the database
            $reqBody = $request->getBody();
            $saveRes = $artist->save($reqBody);
        } catch (\PDOException $e) {
            $responseData['errors'] = "An error occurred. Please try again or contact sys admin";

            return json_encode($responseData, JSON_THROW_ON_ERROR | JSON_INVALID_UTF8_IGNORE);
        }
        if (!$saveRes) {
            $responseData['errors'] = "An error occurred. Please try again or contact sys admin";
            return json_encode($responseData, JSON_THROW_ON_ERROR | JSON_INVALID_UTF8_IGNORE);
        }

        $response->setStatusCode(200);
        return json_encode($saveRes, JSON_THROW_ON_ERROR | JSON_INVALID_UTF8_IGNORE);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return false|string
     * @throws \JsonException
     */
    public function delete(Request $request, Response $response)
    {

        $validation = $request->validate([
            'ArtistId'      => 'required'
        ]);
        if (!$validation ) {
            $responseData['errors'] = $request->getValidationErrors();
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }
        $reqBody = $request->getBody();
        $deleteRes = Artist::delete($reqBody['ArtistId']);
        if (!$deleteRes) {
            $responseData['errors'] = ['Artist was not deleted'];
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_THROW_ON_ERROR | JSON_INVALID_UTF8_IGNORE);
        }

        if ($deleteRes === '23000') {
            $responseData['errors'] = ['Artist cannot be deleted due to relation'];
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_THROW_ON_ERROR | JSON_INVALID_UTF8_IGNORE);
        }
        $response->setStatusCode(200);
        return json_encode('Artist was deleted', JSON_THROW_ON_ERROR | JSON_INVALID_UTF8_IGNORE);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return false|string
     * @throws \JsonException
     */
    public function all(Request $request, Response $response)
    {
        $reqBody = $request->getBody();
        $validation = $request->validate([
            'page'      => 'int',
            'perPage'   => 'int'
        ]);

        if (!$validation ) {
            $responseData['errors'] = $request->getValidationErrors();
            $response->setStatusCode(400);
            return json_encode($responseData, JSON_INVALID_UTF8_IGNORE);
        }
        if (isset($reqBody['page'])) {
            $artists = ArtistRepository::allPaginated($reqBody['perPage'] ?? 25, $reqBody['page'] ?? 1);
            $response->setStatusCode(200);
            return json_encode($artists, JSON_INVALID_UTF8_IGNORE);
        }

        $artists = Artist::all();
        $response->setStatusCode(200);
        return json_encode($artists, JSON_INVALID_UTF8_IGNORE);
    }

}