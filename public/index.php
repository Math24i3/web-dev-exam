<?php
    use app\core\Application;

    require_once __DIR__ . '/../vendor/autoload.php';

    // dirname : takes the current directory of the directory ./ -> public
    $dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__));
    $dotenv->load();

    $config = [
        'db' => [
            'dsn' => $_ENV['APP_ENV'] = 'local'         ? $_ENV['DB_DSN_L']         : $_ENV['DB_DSN'],
            'user' => $_ENV['APP_ENV'] = 'local'        ? $_ENV['DB_USER_L']        : $_ENV['DB_USER'],
            'password' => $_ENV['APP_ENV'] = 'local'    ? $_ENV['DB_PASSWORD_L']    : $_ENV['DB_PASSWORD']
        ]
    ];
    // CSRF TOKEN
    $_SESSION['token'] = bin2hex(random_bytes(35));

    // The entrance to the application
    $app = new Application(dirname(__DIR__), $config);
    include_once '../routes/web.php';
    include_once '../routes/api.php';
    $app->run();