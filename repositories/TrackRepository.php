<?php


namespace app\repositories;


use app\core\Application;
use app\models\Album;
use app\models\Artist;
use app\models\Genre;
use app\models\MediaType;
use app\models\Track;
use PDO;

/**
 * Class TrackRepository
 * @package app\repositories
 */
class TrackRepository
{

    /**
     * TrackRepository constructor.
     * @param Track $track
     */
    public function __construct()
    {

    }

    /**
     * Returns a list with tracks an their relations
     * @param $page
     * @param $perPage
     * @return array
     */
    public static function allWithRelations($perPage, $page = 0): array
    {
        // positioning
        $start = ( $page > 1 ) ? ( $page * $perPage ) - $perPage : 0;
        $sqlQuery = "
            SELECT track.TrackId, track.Name, track.AlbumId, 
            track.MediaTypeId, track.GenreId, track.Composer, 
            track.Milliseconds, track.Bytes, track.UnitPrice, 
            album.Title, album.ArtistId, mediatype.Name as MediaTypeName, 
            artist.Name as ArtistName, genre.Name as GenreName
            FROM ((((track 
            INNER JOIN album ON track.AlbumId = album.AlbumId) 
            INNER JOIN mediatype ON track.MediaTypeId = mediatype.MediaTypeId)
            INNER JOIN artist ON album.ArtistId = artist.ArtistId)
            INNER JOIN genre ON track.GenreId = genre.GenreId)
            LIMIT {$start}, {$perPage};
        ";
        $statement = Application::$app->dbConn->pdo->query($sqlQuery);

        $tracks = array();
        while (($row = $statement->fetch(PDO::FETCH_ASSOC)) !== false) {
            $track = new Track();
            $track->setTrackId($row['TrackId'] ?? null);
            $track->setName($row['Name'] ?? null);
            $track->setAlbumId($row['AlbumId'] ?? null);
            $track->setMediaTypeId($row['MediaTypeId'] ?? null);
            $track->setGenreId($row['GenreId'] ?? null);
            $track->setComposer($row['Composer'] ?? null);
            $track->setMilliseconds($row['Milliseconds'] ?? null);
            $track->setBytes($row['Bytes'] ?? null);
            $track->setUnitPrice($row['UnitPrice'] ?? null);

            $mediaType = new MediaType($row['MediaTypeId'] ?? null, $row['MediaTypeName'] ?? null);
            $track->setMediaType($mediaType);

            $genre = new Genre($row['GenreId'] ?? null, $row['GenreName'] ?? null);
            $track->setGenre($genre);

            $album = new Album();
            $album->setAlbumId($row['AlbumId'] ?? null);
            $album->setTitle($row['Title'] ?? null);
            $album->setArtistId($row['ArtistId'] ?? null);

            $artist = new Artist();
            $artist->setArtistId($row['ArtistId'] ?? null);
            $artist->setName($row['ArtistName'] ?? null);

            $album->setArtist($artist);

            $track->setAlbum($album);

            $tracks['tracks'][] = $track;
        }
        $total = Application::$app->dbConn->pdo->query( "SELECT COUNT(*) FROM track;" )->fetch()['COUNT(*)'];
        $pages = ceil( $total / $perPage );
        $tracks['pages'] = $pages;
        return $tracks;
    }


}