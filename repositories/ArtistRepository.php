<?php


namespace app\repositories;


use app\core\Application;
use app\models\Artist;
use PDO;

class ArtistRepository
{

    /**
     * ArtistRepository constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param $perPage
     * @param int $page
     * @return array
     */
    public static function allPaginated($perPage, $page = 0)
    {
        // positioning
        $start = ( $page > 1 ) ? ( $page * $perPage ) - $perPage : 0;
        $sqlQuery = "SELECT * FROM artist LIMIT {$start}, {$perPage};";
        $statement = Application::$app->dbConn->pdo->query($sqlQuery);

        $artists = array();
        while (($row = $statement->fetch(PDO::FETCH_ASSOC)) !== false) {
            $artist = new Artist();
            $artist->setArtistId($row['ArtistId'] ?? null);
            $artist->setName($row['Name'] ?? null);
            $artists['artists'][] = $artist;
        }

        $total = Application::$app->dbConn->pdo->query( "SELECT COUNT(*) FROM artist;" )->fetch()['COUNT(*)'];
        $pages = ceil( $total / $perPage );
        $artists['pages'] = $pages;
        return $artists;
    }
}