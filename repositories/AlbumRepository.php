<?php


namespace app\repositories;


use app\core\Application;
use app\models\Album;
use app\models\Artist;
use PDO;

class AlbumRepository
{

    /**
     * AlbumRepository constructor.
     */
    public function __construct()
    {
    }

    public static function allWithRelations($perPage, $page = 0): array
    {
        // positioning
        $start = ( $page > 1 ) ? ( $page * $perPage ) - $perPage : 0;
        $sqlQuery = "
            SELECT album.AlbumId, album.Title, album.ArtistId, artist.Name
            FROM (album 
            INNER JOIN artist ON album.ArtistId = artist.ArtistId)
            LIMIT {$start}, {$perPage};
        ";
        $statement = Application::$app->dbConn->pdo->query($sqlQuery);

        $albums = array();
        while (($row = $statement->fetch(PDO::FETCH_ASSOC)) !== false) {
            $album = new Album();
            $album->setAlbumId($row['AlbumId'] ?? null);
            $album->setTitle($row['Title'] ?? null);
            $album->setArtistId($row['ArtistId'] ?? null);

            $artist = new Artist();
            $artist->setArtistId($row['ArtistId'] ?? null);
            $artist->setName($row['Name'] ?? null);

            $album->setArtist($artist);
            $albums['albums'][] = $album;
        }
        $total = Application::$app->dbConn->pdo->query( "SELECT COUNT(*) FROM album;" )->fetch()['COUNT(*)'];
        $pages = ceil( $total / $perPage );
        $albums['pages'] = $pages;
        return $albums;
    }
}