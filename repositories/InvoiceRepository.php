<?php


namespace app\repositories;


use app\core\Application;
use app\models\Invoice;
use app\models\InvoiceLine;
use PDO;

class InvoiceRepository
{

    private static function insertInvoiceLine($data) {
        $pdo = Application::$app->dbConn->pdo;
        $sqlQuery = "INSERT INTO invoiceline (
                     InvoiceId, TrackId, UnitPrice, Quantity) 
                     VALUES (?,?,?,?)";

        $statement = $pdo->prepare($sqlQuery);

        $statement->execute($data);

        return $pdo->lastInsertId();
    }

    public static function insertInvoice($data): bool
    {
        $pdo = Application::$app->dbConn->pdo;
        $sqlQuery = "INSERT INTO invoice (
                     CustomerId, InvoiceDate, BillingAddress, 
                     BillingCity, BillingState, BillingCountry, 
                     BillingPostalCode, Total) 
                     VALUES (?,?,?,?,?,?,?,?);";

        $statement = $pdo->prepare($sqlQuery);

        $insertData = array();
        foreach ($data as $key => $value) {
            if (in_array($key, Invoice::getProperties(), true)) {
                $insertData[] = $value;
            }
        }
        try {
            $pdo->beginTransaction();
            $statement->execute($insertData);
            $lastInsertId = $pdo->lastInsertId();

            foreach ($data['Tracks'] as $track) {
                if ($track->getTrackId()) {
                    $data = [
                        $lastInsertId,
                        $track->getTrackId(),
                        $track->getUnitPrice(),
                        1
                    ];

                    self::insertInvoiceLine($data);
                }
            }

            $pdo->commit();
        } catch (\PDOException $e) {
            $pdo->rollBack();
            return false;
        }

        return true;
    }

}