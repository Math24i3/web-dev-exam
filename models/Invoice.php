<?php


namespace app\models;


use app\core\Model;

class Invoice extends Model implements \JsonSerializable
{
    private $InvoiceId, $InvoiceDate, $CustomerId, $BillingAddress, $BillingCity, $BillingState, $BillingCountry, $BillingPostalCode, $Total;


    public static array $properties = [
        'InvoiceId', 'InvoiceDate', 'CustomerId', 'BillingAddress', 'BillingCity',
        'BillingState', 'BillingCountry', 'BillingPostalCode', 'Total'
    ];
    /**
     * Invoice constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getInvoiceDate()
    {
        return $this->InvoiceDate;
    }

    /**
     * @param mixed $InvoiceDate
     */
    public function setInvoiceDate($InvoiceDate): void
    {
        $this->InvoiceDate = $InvoiceDate;
    }

    /**
     * @return mixed
     */
    public function getInvoiceId()
    {
        return $this->InvoiceId;
    }

    /**
     * @param mixed $InvoiceId
     */
    public function setInvoiceId($InvoiceId): void
    {
        $this->InvoiceId = $InvoiceId;
    }

    /**
     * @return mixed
     */
    public function getCustomerId()
    {
        return $this->CustomerId;
    }

    /**
     * @param mixed $CustomerId
     */
    public function setCustomerId($CustomerId): void
    {
        $this->CustomerId = $CustomerId;
    }

    /**
     * @return mixed
     */
    public function getBillingAddress()
    {
        return $this->BillingAddress;
    }

    /**
     * @param mixed $BillingAddress
     */
    public function setBillingAddress($BillingAddress): void
    {
        $this->BillingAddress = $BillingAddress;
    }

    /**
     * @return mixed
     */
    public function getBillingCity()
    {
        return $this->BillingCity;
    }

    /**
     * @param mixed $BillingCity
     */
    public function setBillingCity($BillingCity): void
    {
        $this->BillingCity = $BillingCity;
    }

    /**
     * @return mixed
     */
    public function getBillingState()
    {
        return $this->BillingState;
    }

    /**
     * @param mixed $BillingState
     */
    public function setBillingState($BillingState): void
    {
        $this->BillingState = $BillingState;
    }

    /**
     * @return mixed
     */
    public function getBillingCountry()
    {
        return $this->BillingCountry;
    }

    /**
     * @param mixed $BillingCountry
     */
    public function setBillingCountry($BillingCountry): void
    {
        $this->BillingCountry = $BillingCountry;
    }

    /**
     * @return mixed
     */
    public function getBillingPostalCode()
    {
        return $this->BillingPostalCode;
    }

    /**
     * @param mixed $BillingPostalCode
     */
    public function setBillingPostalCode($BillingPostalCode): void
    {
        $this->BillingPostalCode = $BillingPostalCode;
    }

    /**
     * @return array|string[]
     */
    public static function getProperties(): array
    {
        return self::$properties;
    }

    /**
     * @param array|string[] $properties
     */
    public static function setProperties(array $properties): void
    {
        self::$properties = $properties;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->Total;
    }

    /**
     * @param mixed $Total
     */
    public function setTotal($Total): void
    {
        $this->Total = $Total;
    }


    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}