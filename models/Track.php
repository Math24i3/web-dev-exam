<?php


namespace app\models;


use app\core\Application;
use PDO;

/**
 * Class Track
 * @package app\models
 */
class Track implements \JsonSerializable
{

    private $TrackId, $Name, $AlbumId, $MediaTypeId, $GenreId, $Composer, $Milliseconds, $Bytes, $UnitPrice, $Album, $MediaType, $Genre;

    /**
     * Track constructor.
     */
    public function __construct()
    {
    }

    public function save(array $data)
    {
        if (empty($data)) {
            return [
                'status' => false,
                'error' => 'Nothing to insert.'
            ];
        }
        foreach ($data as $key => $value) {
            if (!array_key_exists($key, get_class_vars(__CLASS__))) {
                return [
                    'status' => false,
                    'error' => $key . ' is not a property of Customer'
                ];
            }
        }

        // preparing the statement by inserting the table and implode the array of attributes to a string with comma
        $statement = Application::$app->dbConn->pdo->prepare("INSERT INTO track(". implode(', ', array_keys($data)).") 
                                            VALUES ('". implode("', '", $data)."');");

        return $statement->execute();

    }

    /**
     * @return mixed
     */
    public function getGenre()
    {
        return $this->Genre;
    }

    /**
     * @param mixed $Genre
     */
    public function setGenre($Genre): void
    {
        $this->Genre = $Genre;
    }

    /**
     * @return mixed
     */
    public function getAlbum()
    {
        return $this->Album;
    }

    /**
     * @param mixed $Album
     */
    public function setAlbum($Album): void
    {
        $this->Album = $Album;
    }

    /**
     * @return mixed
     */
    public function getMediaType()
    {
        return $this->MediaType;
    }

    /**
     * @param mixed $MediaType
     */
    public function setMediaType($MediaType): void
    {
        $this->MediaType = $MediaType;
    }

    public static function find($id)
    {
        $sqlQuery = "SELECT * FROM track WHERE TrackId = ?";
        $statement = Application::$app->dbConn->pdo->prepare($sqlQuery);
        $statement->execute([$id]);
        return $statement->fetchObject(__CLASS__);
    }

    public static function delete($id)
    {
        $sqlQuery = "DELETE FROM track WHERE TrackId = ?;";
        $statement = Application::$app->dbConn->pdo->prepare($sqlQuery);
        try {
            $statement->execute([$id]);
        } catch (\PDOException $e) {
            return $e->getCode();
        }
        return true;


    }

    /**
     *
     */
    public static function update($id, $data) {
        unset($data['TrackId']);
        if (empty($data)) {
            return [
                'status' => false,
                'error' => 'Nothing to insert.'
            ];
        }
        foreach ($data as $key => $value) {
            if (!array_key_exists($key, get_class_vars(__CLASS__))) {
                return [
                    'status' => false,
                    'error' => $key . ' is not a property of Track'
                ];
            }
        }
        // preparing the statement by inserting the table and implode the array of attributes
        $statement = Application::$app->dbConn->pdo->prepare(
            "UPDATE track SET " . (implode('=?, ', array_keys($data))) . "=? WHERE TrackId=?;");

        $values = array_values($data);
        $values[] = $id;

        return $statement->execute($values);

    }


    public static function all()
    {
        $sqlQuery = "SELECT * FROM track;";
        $statement = Application::$app->dbConn->pdo->query($sqlQuery);
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    /**
     * @return mixed
     */
    public function getTrackId()
    {
        return $this->TrackId;
    }

    /**
     * @param mixed $TrackId
     */
    public function setTrackId($TrackId): void
    {
        $this->TrackId = $TrackId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     * @param mixed $Name
     */
    public function setName($Name): void
    {
        $this->Name = $Name;
    }

    /**
     * @return mixed
     */
    public function getAlbumId()
    {
        return $this->AlbumId;
    }

    /**
     * @param mixed $AlbumId
     */
    public function setAlbumId($AlbumId): void
    {
        $this->AlbumId = $AlbumId;
    }

    /**
     * @return mixed
     */
    public function getMediaTypeId()
    {
        return $this->MediaTypeId;
    }

    /**
     * @param mixed $MediaTypeId
     */
    public function setMediaTypeId($MediaTypeId): void
    {
        $this->MediaTypeId = $MediaTypeId;
    }

    /**
     * @return mixed
     */
    public function getGenreId()
    {
        return $this->GenreId;
    }

    /**
     * @param mixed $GenreId
     */
    public function setGenreId($GenreId): void
    {
        $this->GenreId = $GenreId;
    }

    /**
     * @return mixed
     */
    public function getComposer()
    {
        return $this->Composer;
    }

    /**
     * @param mixed $Composer
     */
    public function setComposer($Composer): void
    {
        $this->Composer = $Composer;
    }

    /**
     * @return mixed
     */
    public function getMilliseconds()
    {
        return $this->Milliseconds;
    }

    /**
     * @param mixed $Milliseconds
     */
    public function setMilliseconds($Milliseconds): void
    {
        $this->Milliseconds = $Milliseconds;
    }

    /**
     * @return mixed
     */
    public function getBytes()
    {
        return $this->Bytes;
    }

    /**
     * @param mixed $Bytes
     */
    public function setBytes($Bytes): void
    {
        $this->Bytes = $Bytes;
    }

    /**
     * @return mixed
     */
    public function getUnitPrice()
    {
        return $this->UnitPrice;
    }

    /**
     * @param mixed $UnitPrice
     */
    public function setUnitPrice($UnitPrice): void
    {
        $this->UnitPrice = $UnitPrice;
    }


}