<?php


namespace app\models;


use app\core\Application;
use PDO;

/**
 * Class Artist
 * @package app\models
 */
class Artist implements \JsonSerializable
{

    /**
     * @var
     */
    /**
     * @var
     */
    private $ArtistId, $Name;

    /**
     * Artist constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function find($id)
    {
        $sqlQuery = "SELECT * FROM artist WHERE ArtistId = ?";
        $statement = Application::$app->dbConn->pdo->prepare($sqlQuery);
        $statement->execute([$id]);
        return $statement->fetchObject(__CLASS__);
    }

    /**
     * @param $id
     * @return bool|int|mixed
     */
    public static function delete($id)
    {
        $sqlQuery = "DELETE FROM artist WHERE ArtistId = ?;";
        $statement = Application::$app->dbConn->pdo->prepare($sqlQuery);
        try {
            $statement->execute([$id]);
        } catch (\PDOException $e) {
            return $e->getCode();
        }
        return true;
    }

    /**
     *
     */
    public static function update($id, $data) {
        unset($data['ArtistId']);
        if (empty($data)) {
            return [
                'status' => false,
                'error' => 'Nothing to insert.'
            ];
        }
        foreach ($data as $key => $value) {
            if (!array_key_exists($key, get_class_vars(__CLASS__))) {
                return [
                    'status' => false,
                    'error' => $key . ' is not a property of Artist'
                ];
            }
        }
        // preparing the statement by inserting the table and implode the array of attributes
        $statement = Application::$app->dbConn->pdo->prepare(
            "UPDATE artist SET " . (implode('=?, ', array_keys($data))) . "=? WHERE ArtistId=?;");

        $values = array_values($data);
        $values[] = $id;

        return $statement->execute($values);

    }

    /**
     * @return array
     */
    public static function all()
    {
        $sqlQuery = "SELECT * FROM artist;";
        $statement = Application::$app->dbConn->pdo->query($sqlQuery);
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }


    /**
     * @return array|mixed
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    /**
     * @return mixed
     */
    public function getArtistId()
    {
        return $this->ArtistId;
    }

    /**
     * @param mixed $ArtistId
     */
    public function setArtistId($ArtistId): void
    {
        $this->ArtistId = $ArtistId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     * @param mixed $Name
     */
    public function setName($Name): void
    {
        $this->Name = $Name;
    }

    /**
     * @param $data
     * @return array|bool
     */
    public function save($data) {
        if (empty($data)) {
            return [
                'status' => false,
                'error' => 'Nothing to insert.'
            ];
        }
        foreach ($data as $key => $value) {
            if (!array_key_exists($key, get_class_vars(__CLASS__))) {
                return [
                    'status' => false,
                    'error' => $key . ' is not a property of Artist'
                ];
            }
        }

        // preparing the statement by inserting the table and implode the array of attributes to a string with comma
        $statement = Application::$app->dbConn->pdo->prepare("INSERT INTO artist(". implode(', ', array_keys($data)).") 
                                            VALUES ('". implode("', '", $data)."');");

        return $statement->execute();
    }


}