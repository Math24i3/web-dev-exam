<?php


namespace app\models;


use app\core\Application;
use app\core\ORMModel;

class Admin extends ORMModel
{

    private $Password;

    /**
     * Admin constructor.
     */
    public function __construct()
    {
    }

    public static function getTheOnlyAdminInTheDatabaseByTheHashedPasswordWhichIsImpossibleToExStract()
    {
        $sqlQuery = "SELECT * FROM admin LIMIT 1;";
        $statement = Application::$app->dbConn->pdo->prepare($sqlQuery);
        $statement->execute();
        return $statement->fetchObject(__CLASS__);
    }

    public function tableName(): string
    {
        return 'admin';
    }

    public function attributes(): array
    {
        return ['Password'];
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->Password;
    }


    /**
     * @param mixed $Password
     */
    public function setPassword($Password): void
    {
        $this->Password = $Password;
    }
}