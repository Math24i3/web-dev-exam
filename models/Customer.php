<?php


namespace app\models;


use app\core\Application;
use app\core\ORMModel;

/**
 * Class Customer
 * @package app\models
 */
class Customer implements \JsonSerializable
{

    private $CustomerId, $FirstName, $LastName, $Password, $Company, $Address, $City, $State, $Country, $PostalCode, $Phone, $Fax, $Email;

    /**
     * Customer constructor.
     */
    public function __construct()
    {
    }


    public static function getByEmail($email){
        $sqlQuery = "SELECT * FROM customer WHERE Email = ?";
        $statement = Application::$app->dbConn->pdo->prepare($sqlQuery);
        $statement->execute([$email]);
        return $statement->fetchObject(__CLASS__);

    }

    public static function find($id)
    {
        $sqlQuery = "SELECT * FROM customer WHERE CustomerId = ?";
        $statement = Application::$app->dbConn->pdo->prepare($sqlQuery);
        $statement->execute([$id]);
        return $statement->fetchObject(__CLASS__);
    }

    /**
     *
     */
    public static function update($id, $data) {
        unset($data['CustomerId']);
        if (empty($data)) {
            return [
                'status' => false,
                'error' => 'Nothing to insert.'
            ];
        }
        foreach ($data as $key => $value) {
            if (!array_key_exists($key, get_class_vars(__CLASS__))) {
                return [
                    'status' => false,
                    'error' => $key . ' is not a property of Customer'
                ];
            }
        }
        // preparing the statement by inserting the table and implode the array of attributes
        $statement = Application::$app->dbConn->pdo->prepare(
            "UPDATE customer SET " . (implode('=?, ', array_keys($data))) . "=? WHERE CustomerId=?;");

        $values = array_values($data);
        $values[] = $id;

        return $statement->execute($values);

    }

    public function save($data) {
        if (empty($data)) {
            return [
                'status' => false,
                'error' => 'Nothing to insert.'
            ];
        }
        foreach ($data as $key => $value) {
            if (!array_key_exists($key, get_class_vars(__CLASS__))) {
                return [
                    'status' => false,
                    'error' => $key . ' is not a property of Customer'
                ];
            }
        }
        $data['Password'] = password_hash($data['Password'], PASSWORD_DEFAULT);

        // preparing the statement by inserting the table and implode the array of attributes to a string with comma
        $statement = Application::$app->dbConn->pdo->prepare("INSERT INTO customer(". implode(', ', array_keys($data)).") 
                                            VALUES ('". implode("', '", $data)."');");

        return $statement->execute();
    }

    /**
     * @return mixed
     */
    public function getCustomerId()
    {
        return $this->CustomerId;
    }

    /**
     * @param mixed $CustomerId
     */
    public function setCustomerId($CustomerId): void
    {
        $this->CustomerId = $CustomerId;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->FirstName;
    }

    /**
     * @param mixed $FirstName
     */
    public function setFirstName($FirstName): void
    {
        $this->FirstName = $FirstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->LastName;
    }

    /**
     * @param mixed $LastName
     */
    public function setLastName($LastName): void
    {
        $this->LastName = $LastName;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->Password;
    }

    /**
     * @param mixed $Password
     */
    public function setPassword($Password): void
    {
        $this->Password = $Password;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->Company;
    }

    /**
     * @param mixed $Company
     */
    public function setCompany($Company): void
    {
        $this->Company = $Company;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->Address;
    }

    /**
     * @param mixed $Address
     */
    public function setAddress($Address): void
    {
        $this->Address = $Address;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->City;
    }

    /**
     * @param mixed $City
     */
    public function setCity($City): void
    {
        $this->City = $City;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->State;
    }

    /**
     * @param mixed $State
     */
    public function setState($State): void
    {
        $this->State = $State;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->Country;
    }

    /**
     * @param mixed $Country
     */
    public function setCountry($Country): void
    {
        $this->Country = $Country;
    }

    /**
     * @return mixed
     */
    public function getPostalCode()
    {
        return $this->PostalCode;
    }

    /**
     * @param mixed $PostalCode
     */
    public function setPostalCode($PostalCode): void
    {
        $this->PostalCode = $PostalCode;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->Phone;
    }

    /**
     * @param mixed $Phone
     */
    public function setPhone($Phone): void
    {
        $this->Phone = $Phone;
    }

    /**
     * @return mixed
     */
    public function getFax()
    {
        return $this->Fax;
    }

    /**
     * @param mixed $Fax
     */
    public function setFax($Fax): void
    {
        $this->Fax = $Fax;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->Email;
    }

    /**
     * @param mixed $Email
     */
    public function setEmail($Email): void
    {
        $this->Email = $Email;
    }


    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}