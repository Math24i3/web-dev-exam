<?php


namespace app\models;


use app\core\Model;

class InvoiceLine extends Model implements \JsonSerializable
{

    private $InvoiceLineId, $InvoiceId, $TrackId, $UnitPrice, $Quantity;

    public static array $properties = [
        'InvoiceLineId', 'InvoiceId', 'TrackId', 'UnitPrice', 'Quantity'
    ];

    /**
     * InvoiceLine constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getInvoiceLineId()
    {
        return $this->InvoiceLineId;
    }

    /**
     * @param mixed $InvoiceLineId
     */
    public function setInvoiceLineId($InvoiceLineId): void
    {
        $this->InvoiceLineId = $InvoiceLineId;
    }

    /**
     * @return mixed
     */
    public function getInvoiceId()
    {
        return $this->InvoiceId;
    }

    /**
     * @param mixed $InvoiceId
     */
    public function setInvoiceId($InvoiceId): void
    {
        $this->InvoiceId = $InvoiceId;
    }

    /**
     * @return mixed
     */
    public function getTrackId()
    {
        return $this->TrackId;
    }

    /**
     * @param mixed $TrackId
     */
    public function setTrackId($TrackId): void
    {
        $this->TrackId = $TrackId;
    }

    /**
     * @return mixed
     */
    public function getUnitPrice()
    {
        return $this->UnitPrice;
    }

    /**
     * @param mixed $UnitPrice
     */
    public function setUnitPrice($UnitPrice): void
    {
        $this->UnitPrice = $UnitPrice;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->Quantity;
    }

    /**
     * @param mixed $Quantity
     */
    public function setQuantity($Quantity): void
    {
        $this->Quantity = $Quantity;
    }

    /**
     * @return array
     */
    public static function getProperties(): array
    {
        return self::$properties;
    }

    /**
     * @param array $properties
     */
    public static function setProperties(array $properties): void
    {
        self::$properties = $properties;
    }



    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}