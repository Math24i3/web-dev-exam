<?php


namespace app\models;


use app\core\Application;
use PDO;

/**
 * Class Album
 * @package app\models
 */
class Album implements \JsonSerializable
{

    /**
     * @var
     */
    /**
     * @var
     */
    /**
     * @var
     */
    /**
     * @var
     */
    private $AlbumId, $Title, $ArtistId, $Artist;

    /**
     * Album constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param $id
     * @return bool|int|mixed
     */
    public static function delete($id)
    {
        $sqlQuery = "DELETE FROM album WHERE AlbumId = ?;";
        $statement = Application::$app->dbConn->pdo->prepare($sqlQuery);
        try {
            $statement->execute([$id]);
        } catch (\PDOException $e) {
            return $e->getCode();
        }
        return true;
    }

    /**
     *
     */
    public static function update($id, $data) {
        unset($data['AlbumId']);
        if (empty($data)) {
            return [
                'status' => false,
                'error' => 'Nothing to insert.'
            ];
        }
        foreach ($data as $key => $value) {
            if (!array_key_exists($key, get_class_vars(__CLASS__))) {
                return [
                    'status' => false,
                    'error' => $key . ' is not a property of Album'
                ];
            }
        }
        // preparing the statement by inserting the table and implode the array of attributes
        $statement = Application::$app->dbConn->pdo->prepare(
            "UPDATE album SET " . (implode('=?, ', array_keys($data))) . "=? WHERE AlbumId=?;");

        $values = array_values($data);
        $values[] = $id;

        return $statement->execute($values);

    }

    /**
     * @return Artist
     */
    public function getArtist()
    {
        return $this->Artist;
    }

    /**
     * @param Artist $Artist
     */
    public function setArtist(Artist $Artist): void
    {
        $this->Artist = $Artist;
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function find($id)
    {
        $sqlQuery = "SELECT * FROM album WHERE AlbumId = ?";
        $statement = Application::$app->dbConn->pdo->prepare($sqlQuery);
        $statement->execute([$id]);
        return $statement->fetchObject(__CLASS__);
    }

    /**
     * @return array
     */
    public static function all()
    {
        $sqlQuery = "SELECT * FROM album;";
        $statement = Application::$app->dbConn->pdo->query($sqlQuery);
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @return array|mixed
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    /**
     * @return mixed
     */
    public function getAlbumId()
    {
        return $this->AlbumId;
    }

    /**
     * @param mixed $AlbumId
     */
    public function setAlbumId($AlbumId): void
    {
        $this->AlbumId = $AlbumId;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->Title;
    }

    /**
     * @param mixed $Title
     */
    public function setTitle($Title): void
    {
        $this->Title = $Title;
    }

    /**
     * @return mixed
     */
    public function getArtistId()
    {
        return $this->ArtistId;
    }

    /**
     * @param mixed $ArtisId
     */
    public function setArtistId($ArtistId): void
    {
        $this->ArtistId = $ArtistId;
    }

    /**
     * @param $data
     * @return array|bool
     */
    public function save($data) {
        if (empty($data)) {
            return [
                'status' => false,
                'error' => 'Nothing to insert.'
            ];
        }
        foreach ($data as $key => $value) {
            if (!array_key_exists($key, get_class_vars(__CLASS__))) {
                return [
                    'status' => false,
                    'error' => $key . ' is not a property of Album'
                ];
            }
        }

        // preparing the statement by inserting the table and implode the array of attributes to a string with comma
        $statement = Application::$app->dbConn->pdo->prepare("INSERT INTO album(". implode(', ', array_keys($data)).") 
                                            VALUES ('". implode("', '", $data)."');");

        return $statement->execute();
    }


}