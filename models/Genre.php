<?php


namespace app\models;


use app\core\Application;
use PDO;

class Genre implements \JsonSerializable
{

    private $GenreId, $Name;

    /**
     * Genre constructor.
     * @param $GenreId
     * @param $Name
     */
    public function __construct($GenreId, $Name)
    {
        $this->GenreId = $GenreId;
        $this->Name = $Name;
    }

    /**
     * get all mediaType lookups
     * @return array
     */
    public static function all()
    {
        $sqlQuery = "SELECT * FROM genre;";
        $statement = Application::$app->dbConn->pdo->query($sqlQuery);
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @return mixed
     */
    public function getGenreId()
    {
        return $this->GenreId;
    }

    /**
     * @param mixed $GenreId
     */
    public function setGenreId($GenreId): void
    {
        $this->GenreId = $GenreId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     * @param mixed $Name
     */
    public function setName($Name): void
    {
        $this->Name = $Name;
    }




    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}