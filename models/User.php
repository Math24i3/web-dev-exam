<?php
namespace app\models;

use app\core\Model;
use app\core\ORMModel;

/**
 *
 */
class User extends ORMModel
{

    /**
     * @var string
     */
    public string $name;
    /**
     * @var string
     */
    public string $email;
    /**
     * @var string
     */
    public string $password;

    /**
     * @return string
     */
    public function tableName(): string
    {
        return 'users';
    }


    /**
     * attributes that must be assigned to the user / table
     * @return array
     */
    public function attributes(): array
    {
        return ['name', 'email', 'password'];
    }

    /**
     * the save function
     */
    public function save()
    {
        $this->password = password_hash($this->password, PASSWORD_DEFAULT);

        return parent::save();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }
}