<?php


namespace app\models;


use app\core\Application;
use PDO;

class MediaType implements \JsonSerializable
{

    private $MediaTypeId, $Name;

    /**
     * MediaType constructor.
     * @param $MediaTypeId
     * @param $Name
     */
    public function __construct($MediaTypeId, $Name)
    {
        $this->MediaTypeId = $MediaTypeId;
        $this->Name = $Name;
    }

    /**
     * get all genre lookups
     * @return array
     */
    public static function all()
    {
        $sqlQuery = "SELECT * FROM mediatype;";
        $statement = Application::$app->dbConn->pdo->query($sqlQuery);
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }


    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}