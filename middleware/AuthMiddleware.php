<?php


namespace app\middleware;


use app\core\Application;
use app\core\exceptions\ForbiddenException;
use app\core\middlewares\BaseMiddleware;

class AuthMiddleware extends BaseMiddleware
{

    protected array $actions = [];
    protected ?string $user;

    /**
     * AuthMiddleware constructor.
     * @param array $actions
     * @param string|null $user
     */
    public function __construct(array $actions = [], string $user = null)
    {
        $this->actions = $actions;
        $this->user = $user;
    }

    public function exec()
    {
        if(Application::isGuest()) {
            if(empty($this->actions) || in_array(Application::$app->controller->action, $this->actions, true)) {
                throw new ForbiddenException();
            }
        }
        if (isset($this->user) && !Application::$app->session->get($this->user)) {
            if(empty($this->actions) || in_array(Application::$app->controller->action, $this->actions, true)) {
                throw new ForbiddenException();
            }
        }
    }
}