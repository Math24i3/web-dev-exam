<?php


use app\core\Application;
use app\core\ViewHandler;

/** @var $customer */

/** @var  $this ViewHandler */
$this->title = 'Cart';

$totalPrice = 0;

?>

<div class="container">
    <h1 >Cart</h1>
    <section class="base-card" id="profile-section">

        <div class="flex-container-card">
        <?php if (Application::$app->session->get('cart')):
        foreach (Application::$app->session->get('cart') as $key => $track):
            $totalPrice += $track->getUnitPrice();
            ?>
                <div class="cart-item">
                    <div>
                        <p><?php echo htmlspecialchars($track->getName()) ?? ''?></p>
                    </div>
                    <div>
                        <p><?php echo $track->getMilliseconds() ? htmlspecialchars(round($track->getMilliseconds() / 1000 / 60, 2)) : ''?> MIN</p>
                    </div>
                    <div>
                        <p><?php echo htmlspecialchars($track->getUnitPrice()) ?? '' ?> EUR</p>
                    </div>

                    <div>
                        <button onclick="removeFromCart(<?php echo $key?>)">Remove</button>
                    </div>



                </div>

        <?php endforeach; ?>
        <?php endif; ?>
        </div>
        <div class="flex-container">
            <div class="flex-item-left">Total Price: </div>
            <div class="flex-item-right text-right"><?php echo htmlspecialchars($totalPrice) ?? '' ?> EUR</div>
        </div>
        <div class="flex-container">
            <div class="flex-item-left"><button onclick="window.location.assign('/checkout')">Checkout</button></div>
        </div>

    </section>

    <script>
        const removeFromCartUrl = '/api/cart'

        async function removeFromCart(trackId) {
            if (trackId) {
                return await fetch(removeFromCartUrl, {
                    method: 'DELETE',
                    body: JSON.stringify({trackId : trackId})
                }).then(response => response.json())
                    .then((res) => {
                        if (res) {
                            location.reload()
                        }
                    }).catch(err => {
                        return err.message()
                    })
            }
        }
    </script>

</div>
