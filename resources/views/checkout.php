<?php


use app\core\Application;
use app\core\ViewHandler;
use app\models\Customer;

/** @var $customer  */
/** @var $cart  */
/** @var $totalPrice  */

/** @var  $this ViewHandler */
$this->title = 'Checkout';
?>

<div class="container">
    <?php echo $errors['token'] ?? '' ?>
    <h1 id="customer-title" >Checkout</h1>
    <div class="alert alert-success" id="profile-alert" hidden>
        <small></small>
    </div>
    <?php if(isset($errors)): ?>
        <div class="alert alert-danger" id="profile-alert-danger" >
            <small>Could not complete the purchase. Please check below.</small>
        </div>
    <?php endif; ?>
    <section class="base-card">
        <form action="/checkout" method="POST">

            <div class="flex-container">
                <div class="flex-item-left">
                    First name:
                    <input
                            class="float-right <?php if (isset($errors['firstName'][0])) {echo 'is-invalid';} ?>"
                            type="text"
                            id="firstName"
                            name="firstName"
                            value="<?php echo $customer->getFirstName() ?? '' ?>"
                    >
                    <small class="form-invalid">
                        <?php echo $errors['firstName'][0] ?? '' ?>
                    </small>
                </div>
                <div class="flex-item-right">
                    Last name:
                    <input
                            class="float-right <?php if (isset($errors['lastName'][0])) {echo 'is-invalid';} ?>"
                            type="text"
                            id="lastName"
                            name="lastName"
                            required
                            value="<?php echo $customer->getLastName() ?? ''?>"
                    >
                    <small class="form-invalid">
                        <?php echo $errors['lastName'][0] ?? '' ?>
                    </small>
                </div>
            </div>
            <div class="flex-container">
                <div class="flex-item-left">
                    Email:
                    <input
                            class="float-right <?php if (isset($errors['email'][0])) {echo 'is-invalid';} ?>"
                            type="text"
                            id="email"
                            name="email"
                            required
                            value="<?php echo $customer->getEmail() ?? ''?>"
                    >
                    <small class="form-invalid">
                        <?php echo $errors['email'][0] ?? '' ?>
                    </small>
                </div>
                <div class="flex-item-right">
                    Address:
                    <input
                            class="float-right <?php if (isset($errors['address'][0])) {echo 'is-invalid';} ?>"
                            type="text"
                            id="address"
                            name="address"
                            value="<?php echo $customer->getAddress() ?? '' ?>"
                    >
                    <small class="form-invalid">
                        <?php echo $errors['address'][0] ?? '' ?>
                    </small>
                </div>
            </div>

            <div class="flex-container">
                <div class="flex-item-left">
                    City:
                    <input
                            class="float-right <?php if (isset($errors['city'][0])) {echo 'is-invalid';} ?>"
                            type="text"
                            id="city"
                            name="city"
                            value="<?php echo $customer->getCity() ?? '' ?>"
                    >
                    <small class="form-invalid">
                        <?php echo $errors['city'][0] ?? '' ?>
                    </small>
                </div>
                <div class="flex-item-right">
                    State:
                    <input
                            class="float-right <?php if (isset($errors['state'][0])) {echo 'is-invalid';} ?>"
                            type="text"
                            id="state"
                            name="state"
                            value="<?php echo $customer->getState() ?? '' ?>"
                    >
                    <small class="form-invalid">
                        <?php echo $errors['state'][0] ?? '' ?>
                    </small>
                </div>
            </div>
            <div class="flex-container">
                <div class="flex-item-left">
                    Country:
                    <input
                            class="float-right <?php if (isset($errors['country'][0])) {echo 'is-invalid';} ?>"
                            type="text"
                            id="country"
                            name="country"
                            value="<?php echo $customer->getCountry() ?? '' ?>"
                    >
                    <small class="form-invalid">
                        <?php echo $errors['country'][0] ?? '' ?>
                    </small>
                </div>
                <div class="flex-item-right">
                    Postal:
                    <input
                            class="float-right <?php if (isset($errors['postalCode'][0])) {echo 'is-invalid';} ?>"
                            type="text"
                            id="postalCode"
                            name="postalCode"
                            value="<?php echo $customer->getPostalCode() ?? '' ?>"
                    >
                    <small class="form-invalid">
                        <?php echo $errors['postalCode'][0] ?? '' ?>
                    </small>
                </div>
            </div>
            <div class="flex-container">
                <div class="flex-item-left">Total Price: </div>
                <div class="flex-item-right text-right"><?php echo htmlspecialchars($totalPrice) ?? '' ?> EUR</div>
            </div>
            <div class="flex-container">
                <input type="hidden" name="token" value="<?= $_SESSION['token'] ?? '' ?>">
                <div class="flex-item-left"><button id="btn-checkout-buy" type="submit">Buy</button></div>
                <div class="flex-item-right"><button id="btn-checkout-cancel" type="button" onclick="window.location.assign('/cart')">Cancel</button></div>
            </div>
        </form>
    </section>


</div>