<?php
use app\core\Application;

/** @var $errors */

?>
<div class="container">
    <section class="login-form">
        <?php echo $errors['token'] ?? '' ?>
        <h1>Login</h1>

        <form class="form" action="/login" method="POST">
            <label for="email">Email</label>
            <div class="form-input">
                <input type="text"  name="email" placeholder="Email" class="<?php if (isset($errors['email'][0]) || Application::$app->session->getSessionMessage('error')) {echo 'is-invalid';} ?>" required value=<?= $_POST['email'] ?? ''?>>
                <div>
                    <small class="form-invalid">
                        <?php echo $errors['email'][0] ?? '' ?>
                    </small>
                </div>
            </div>

            <label for="password">Password</label>
            <div class="form-input">
                <input type="password" name="password" placeholder="Password" class="<?php if (isset($errors['password'][0]) || Application::$app->session->getSessionMessage('error')) {echo 'is-invalid';} ?>" required>
                <div>
                    <small class="form-invalid">
                        <?php echo $errors['password'][0] ?? '' ?>
                    </small>
                </div>
                <?php if (Application::$app->session->getSessionMessage('error')): ?>
                    <div>
                        <small class="form-invalid">
                            <?php echo Application::$app->session->getSessionMessage('error') ?>
                        </small>
                    </div>
                <?php endif; ?>
            </div>
            <input type="hidden" name="token" value="<?= $_SESSION['token'] ?? '' ?>">
            <button type="submit">Login</button>

        </form>
    </section>

</div>
