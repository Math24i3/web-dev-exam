<?php


use app\core\ViewHandler;

/** @var $customer */

/** @var  $this ViewHandler */
$this->title = 'Profile'

?>


<div class="container">
    <h1 id="customer-title" ><?php echo $customer->getFirstName() . ' ' . $customer->getLastName() ?></h1>
    <div class="alert alert-success" id="profile-alert" hidden>
        <small></small>
    </div>
    <div class="alert alert-danger" id="profile-alert-danger" hidden>
        <small></small>
    </div>
    <section class="base-card" id="profile-section">
        <div class="flex-container">
            <div class="flex-item-left"><button id="btn-edit">Edit profile</button></div>
            <div class="flex-item-right"><button id="btn-change-pwd">Change password</button></div>
        </div>
        <div class="flex-container">
            <div class="flex-item-left">First name: <strong id="s-firstName"><?php echo htmlspecialchars($customer->getFirstName()) ?></strong></div>
            <div class="flex-item-right">Last name: <strong id="s-lastName"><?php echo htmlspecialchars($customer->getLastName()) ?></strong></div>
        </div>
        <div class="flex-container">
            <div class="flex-item-left">Email: <strong id="s-email"><?php echo htmlspecialchars($customer->getEmail()) ?></strong></div>
            <div class="flex-item-right">Phone: <strong id="s-phone"><?php echo htmlspecialchars($customer->getPhone()) ?></strong></div>
        </div>
        <div class="flex-container">
            <div class="flex-item-left">Fax: <strong id="s-fax"><?php echo htmlspecialchars($customer->getFax()) ?></strong></div>
            <div class="flex-item-right">Address: <strong id="s-address"><?php echo htmlspecialchars($customer->getAddress()) ?></strong></div>
        </div>
        <div class="flex-container">
            <div class="flex-item-left">City: <strong id="s-city"><?php echo htmlspecialchars($customer->getCity()) ?></strong></div>
            <div class="flex-item-right">State: <strong id="s-state"><?php echo htmlspecialchars($customer->getState()) ?></strong></div>
        </div>
        <div class="flex-container">
            <div class="flex-item-left">Country: <strong id="s-country"><?php echo htmlspecialchars($customer->getCountry()) ?></strong></div>
            <div class="flex-item-right">PostalCode: <strong id="s-postalCode"><?php echo htmlspecialchars($customer->getPostalCode()) ?></strong></div>
        </div>
    </section>
    <section class="base-card" id="profile-edit-section" hidden>
        <div class="flex-container">
            <div class="flex-item-left"><button id="btn-profile-save">Save changes</button></div>
            <div class="flex-item-right"><button id="btn-profile-discard">Discard</button></div>
        </div>
        <div class="flex-container">
            <div class="flex-item-left">
                First name: <input class="float-right" type="text" id="firstName" required value="<?php echo $customer->getFirstName() ?>">
            </div>
            <div class="flex-item-right">
                Last name: <input class="float-right" type="text" id="lastName" required value="<?php echo $customer->getLastName() ?>">
            </div>
        </div>
        <div class="flex-container">
            <div class="flex-item-left">
                Email: <input class="float-right" type="text" id="email" required value="<?php echo $customer->getEmail() ?>">
            </div>
            <div class="flex-item-right">
                Phone: <input class="float-right" type="text" id="phone" value="<?php echo $customer->getPhone() ?>">
            </div>
        </div>
        <div class="flex-container">
            <div class="flex-item-left">
                Fax: <input class="float-right" type="text" id="fax" value="<?php echo $customer->getFax() ?>">
            </div>
            <div class="flex-item-right">
                Address: <input class="float-right" type="text" id="address" value="<?php echo $customer->getAddress() ?>">
            </div>
        </div>
        <div class="flex-container">
            <div class="flex-item-left">
                City: <input class="float-right" type="text" id="city" value="<?php echo $customer->getCity() ?>">
            </div>
            <div class="flex-item-right">
                State: <input class="float-right" type="text" id="state" value="<?php echo $customer->getState() ?>">
            </div>
        </div>
        <div class="flex-container">
            <div class="flex-item-left">
                Country: <input class="float-right" type="text" id="country" value="<?php echo $customer->getCountry() ?>">
            </div>
            <div class="flex-item-right">
                Postal: <input class="float-right" type="text" id="postalCode" value="<?php echo $customer->getPostalCode() ?>">
            </div>
        </div>
    </section>

    <section class="base-card" id="change-pwd-section" hidden>
        <div class="flex-container">
            <div class="flex-item-left">
                Old: <input class="float-right" type="password" id="old-pwd" value="" required placeholder="Old password">
            </div>
            <div class="flex-item-left">
                New: <input class="float-right" type="password" id="new-pwd" value="" required placeholder="New password">
            </div>
            <div class="flex-item-left">
                Confirm: <input class="float-right" type="password" id="confirm-pwd" value="" required placeholder="Confirm password">
            </div>
        </div>
        <div class="flex-container">
            <div class="flex-item-left"><button id="btn-pwd-update">Update Password</button></div>
            <div class="flex-item-right"><button id="btn-pwd-discard">Discard</button></div>
        </div>
    </section>


</div>

<script type="module">

$('#btn-edit').click(function () {
    $('#profile-section').hide();
    $('#profile-edit-section').show();
})

$('#btn-profile-discard').click(function () {
    $('#profile-section').show();
    $('#profile-edit-section').hide();
})
$('#btn-pwd-discard').click(function () {
    $('#profile-section').show();
    $('#change-pwd-section').hide();
})

$('#btn-change-pwd').click(function () {
    $('#change-pwd-section').show();
    $('#profile-section').hide();
})

$('#btn-profile-save').click( async function () {
    const data = {
        CustomerId: <?php echo $customer->getCustomerId() ?>,
        FirstName : $('#firstName').val(),
        LastName : $('#lastName').val(),
        Email : $('#email').val(),
        Fax : $('#fax').val(),
        City : $('#city').val(),
        Country : $('#country').val(),
        Phone : $('#phone').val(),
        Address : $('#address').val(),
        State : $('#state').val(),
        PostalCode : $('#postalCode').val(),

    }

    let response = await putData('/api/customer', data);
    if (response && response.success) {
        $('#s-firstName').text(data.FirstName)
        $('#s-lastName').text(data.LastName)
        $('#s-email').text(data.Email)
        $('#s-fax').text(data.Fax)
        $('#s-city').text(data.City)
        $('#s-country').text(data.Country)
        $('#s-phone').text(data.Phone)
        $('#s-address').text(data.Address)
        $('#s-state').text(data.State)
        $('#s-postalCode').text(data.PostalCode)
        $('#customer-title').text(data.FirstName + ' ' + data.LastName)

        $('#profile-alert small').text(response.success ?? 'Customer updated')
        $('#profile-alert').show()

        setTimeout(() => { $('#profile-alert').hide()}, 3000);
    } else if (response && response.errors) {
        let errs = ''
        for (let error in response.errors) {
            errs = errs + ' ' + response.errors[error].toString()
        }
        $('#profile-alert-danger small').text(errs)
        //$('#profile-alert small').text(response.errors ?? 'Error')
        $('#profile-alert-danger').show()
        setTimeout(() => { $('#profile-alert-danger').hide()}, 3000);
    }
    $('#profile-section').show();
    $('#profile-edit-section').hide();
})

$('#btn-pwd-update').click( async function () {
    const data = {
        CustomerId: <?php echo $customer->getCustomerId() ?>,
        Old: $('#old-pwd').val(),
        Password : $('#new-pwd').val(),
        confirm : $('#confirm-pwd').val(),
    }

    let response = await putData('/api/customer/password', data);
    if (response && response.success) {
        $('#profile-alert small').text(response.success ?? 'Password Updated')
        $('#profile-alert').show()
        setTimeout(() => { $('#profile-alert').hide()}, 3000);
        $('#profile-section').show();
        $('#change-pwd-section').hide();
    } else if (response && response.errors) {
        let errs = ''
        for (let error in response.errors) {
            errs = errs + ' ' + response.errors[error].toString()
        }
        $('#profile-alert-danger small').text(errs)
        //$('#profile-alert small').text(response.errors ?? 'Error')
        $('#profile-alert-danger').show()
        setTimeout(() => { $('#profile-alert-danger').hide()}, 3000);
    }
    clearInput()
})


function clearInput() {
    $(':input').val('')
}

async function putData(url = '', data = {}) {
    const response = await fetch(url, {
        method: 'PUT',
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json'
        },
        redirect: 'follow',
        referrerPolicy: 'no-referrer',
        body: JSON.stringify(data)
    }).catch(error => {
        return error.message()
    });
    return response.json();
}


</script>