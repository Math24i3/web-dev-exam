<?php
use app\core\Application;
echo $success ?? '';

?>

</form>

<div class="container">
    <section class="login-form">
        <?php echo $errors['token'] ?? '' ?>
        <h1>Register</h1>

        <form class="form" action="/register" method="POST">
            <label for="firstName">First Name</label>
            <div class="form-input">
                <input type="text"  name="FirstName" placeholder="First name" class="<?php if (isset($errors['FirstName'][0]) || Application::$app->session->getSessionMessage('error')) {echo 'is-invalid';} ?>" required value=<?= $_POST['FirstName'] ?? ''?>>
                <div>
                    <small class="form-invalid">
                        <?php echo $errors['FirstName'][0] ?? '' ?>
                    </small>
                </div>
            </div>

            <label for="lastName">Last Name</label>
            <div class="form-input">
                <input type="text"  name="LastName" placeholder="Last name" class="<?php if (isset($errors['LastName'][0]) || Application::$app->session->getSessionMessage('error')) {echo 'is-invalid';} ?>" required value=<?= $_POST['LastName'] ?? ''?>>
                <div>
                    <small class="form-invalid">
                        <?php echo $errors['LastName'][0] ?? '' ?>
                    </small>
                </div>
            </div>

            <label for="email">Email</label>
            <div class="form-input">
                <input type="text"  name="Email" placeholder="Email" class="<?php if (isset($errors['Email'][0]) || Application::$app->session->getSessionMessage('error')) {echo 'is-invalid';} ?>" required value=<?= $_POST['Email'] ?? ''?>>
                <div>
                    <small class="form-invalid">
                        <?php echo $errors['Email'][0] ?? '' ?>
                    </small>
                </div>
            </div>

            <label for="password">Password</label>
            <div class="form-input">
                <input type="password" name="Password" placeholder="Password" class="<?php if (isset($errors['Password'][0]) || Application::$app->session->getSessionMessage('error')) {echo 'is-invalid';} ?>" required>
                <div>
                    <small class="form-invalid">
                        <?php echo $errors['Password'][0] ?? '' ?>
                    </small>
                </div>
                <?php if (Application::$app->session->getSessionMessage('error')): ?>
                    <div>
                        <small class="form-invalid">
                            <?php echo Application::$app->session->getSessionMessage('error') ?>
                        </small>
                    </div>
                <?php endif; ?>
            </div>

            <label for="confirm">Confirm password</label>
            <div class="form-input">
                <div>
                    <input type="password" name="confirm" placeholder="Confirm password" required>
                </div>
                <div class="border-top row">
                    <input type="hidden" name="token" value="<?= $_SESSION['token'] ?? '' ?>">
                    <button type="submit">Register</button>
                </div>
            </div>
        </form>
    </section>

</div>