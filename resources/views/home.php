<?php


use app\core\Application;
use app\core\ViewHandler;

/** @var  $this ViewHandler */
$this->title = 'Home';


$sessionUsername = '';

if(isset(Application::$app->customer)) {
    $sessionUsername = Application::$app->customer->getFirstName() ?? 'Customer';
} elseif (isset(Application::$app->admin)) {
    $sessionUsername = 'Admin';
}

?>
<div class="container">
    <h1>Welcome <?php echo $sessionUsername ?></h1>

</div>
