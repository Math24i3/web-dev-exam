<?php
use app\core\Application;
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://code.jquery.com/jquery-3.6.0.slim.js" integrity="sha256-HwWONEZrpuoh951cQD1ov2HUK5zA5DwJ1DNUXaM6FsY=" crossorigin="anonymous"></script>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <style><?php require_once __DIR__ .'/../../css/index.css'; ?></style>
    <style><?php require_once __DIR__ .'/../../css/profile.css'; ?></style>
    <title><?php echo $this->title ?></title>
</head>
    <body>
        <nav class="navigation">
            <ul class="navbar ">
                <li class="nav-item">
                    <a class="nav-link" aria-current="page" href="/">Home</a>
                </li>
                <?php if(Application::isGuest()): ?>
                    <li class="nav-item-right">
                        <a class="nav-link" href="/login">Customer Login</a>
                    </li>
                    <li class="nav-item-right">
                        <a class="nav-link" href="/register">Register</a>
                    </li>
                <?php else: ?>
                <li class="nav-item-right">
                    <a class="nav-link" href="/logout">Logout</a>
                </li>
                <?php endif ?>

                <?php if(Application::is('admin')): ?>
                    <li class="nav-item">
                        <a class="nav-link" href="/edit">Edit</a>
                    </li>
                <?php endif; ?>

                <?php if(Application::is('customer')): ?>
                    <li class="nav-item">
                        <a class="nav-link" href="/browse">Browse</a>
                    </li>
                    <li class="nav-item-right">
                        <a class="nav-link" href="/profile">
                            <?php echo Application::$app->customer->getFirstName() . ' Profile' ?? "Profile" ?>
                        </a>
                    </li>
                    <li class="nav-item-right">
                        <a class="nav-link" href="/cart">
                            <?php if(Application::$app->session->get('cart')): ?>
                                Cart: <?php echo count(Application::$app->session->get('cart')) ?>
                            <?php else: ?>
                                Cart
                            <?php endif; ?>
                        </a>
                    </li>
                <?php endif; ?>
                </ul>

        </nav>

        <div class="main">
            <div class="info-box">
                <?php if (Application::$app->session->getSessionMessage('success')): ?>
                    <div class="alert alert-success">

                        <small>
                            <?php echo Application::$app->session->getSessionMessage('success') ?>
                        </small>
                    </div>
                <?php endif; ?>
            </div>


            {{content}}
        </div>
    </body>
</html>