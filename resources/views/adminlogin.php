<?php
use app\core\Application;

/** @var $errors */

?>
<div class="container">
    <section class="login-form">
        <h1>Login</h1>

        <form class="form" action="/adminlogin" method="POST">
            <label for="password">Password</label>
            <div class="form-input">
                <input type="password" name="password" placeholder="Password" class="<?php if (isset($errors['password'][0]) || Application::$app->session->getSessionMessage('error')) {echo 'is-invalid';} ?>" required>
                <div>
                    <small class="form-invalid">
                        <?php echo $errors['password'][0] ?? '' ?>
                    </small>
                </div>
                <?php if (Application::$app->session->getSessionMessage('error')): ?>
                    <div>
                        <small class="form-invalid">
                            <?php echo Application::$app->session->getSessionMessage('error') ?>
                        </small>
                    </div>
                <?php endif; ?>
            </div>
            <div class="">
                <button type="submit">Login Administrator</button>
            </div>
        </form>
    </section>

</div>