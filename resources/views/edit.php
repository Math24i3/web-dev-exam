<?php


use app\core\ViewHandler;

/** @var  $this ViewHandler */
/** @var  string $crudEndpoint */
/** @var  string $postEndpoint */

$this->title = 'Edit'
?>
<div class="container">
    <div class="alert alert-success" id="action-alert-success" hidden>
        <small></small>
    </div>
    <div class="alert alert-danger" id="action-alert-danger" hidden>
        <small></small>
    </div>

    <section class="browse-menu">
        <button id="btn-tracks">Tracks</button>
        <button id="btn-albums">Albums</button>
        <button id="btn-artists">Artists</button>
    </section>

    <section class="add-entry-section" id="add-entry-section">

    </section>

    <section class="browse-section">
        <table class="item-list" id="item-list">

        </table>
        <div class="pagination">
            <button class="float-left" id="btn-prev">&laquo;</button>
            <p id="page-number">1</p>
            <button class="float-right" id="btn-next">&raquo;</button>
        </div>
    </section>
</div>



<script>

    let activeSection = 'tracks'
    const tracksUrl = '/api/tracks?withRelations=true&';
    const albumsUrl = '/api/albums?withRelations=true&';
    const artistsUrl = '/api/artists?';
    const mediaTypesUrl = '/api/mediaTypes';
    const genresUrl = '/api/genres'
    const crudUrl = '<?php echo $crudEndpoint  ?>' // I'm trying something out here... RELAX and enjoy my experimental mindset

    const perPage = 30;
    let pageNumber = 1;
    let maxPages = 1;


    $(`#btn-${activeSection}`).addClass('is-active')

    // This is an efficient way to referer to a variable in the scope by string
    paginateResults(eval(`${activeSection}Url`))
    parseAddUI(activeSection)

    $('#btn-next').click(function () {
        if (pageNumber < maxPages) {
            paginateResults(eval(`${activeSection}Url`), pageNumber +=1)
        }
    });

    $('#btn-prev').click(function () {
        if (pageNumber > 1) {
            paginateResults(eval(`${activeSection}Url`), pageNumber -=1)
        }
    });

    $('#btn-tracks').click(function () {
        $(`#btn-${activeSection}`).removeClass('is-active')
        activeSection = 'tracks'
        $(`#btn-${activeSection}`).addClass('is-active')
        paginateResults(eval(`${activeSection}Url`))
        parseAddUI(activeSection)

    });

    $('#btn-albums').click(function () {
        $(`#btn-${activeSection}`).removeClass('is-active')
        activeSection = 'albums'
        $(`#btn-${activeSection}`).addClass('is-active')
        paginateResults(eval(`${activeSection}Url`))
        parseAddUI(activeSection)
    });

    $('#btn-artists').click(function () {
        $(`#btn-${activeSection}`).removeClass('is-active')
        activeSection = 'artists'
        $(`#btn-${activeSection}`).addClass('is-active')
        paginateResults(eval(`${activeSection}Url`))
        parseAddUI(activeSection)
    });

    async function getSelectorInputs(url) {
        const response = await fetch(url)
        if (response) {

            return response.json()
        }
        return false
    }

    async function parseAddUI(model) {
        $('#add-entry-section').empty()
        switch (model) {
            case 'tracks':
                const albums = await getSelectorInputs('/api/albums');
                const mediaTypes = await getSelectorInputs('/api/media-types');
                const genres = await getSelectorInputs('/api/genres');
                if(albums && mediaTypes && genres) {
                    let addSection = `
                        <input type="text" name="Name" placeholder="Name" required>
                        <input type="text" name="Composer" placeholder="Composer" >
                        <input type="number" name="Milliseconds" placeholder="Milliseconds" required>
                        <input type="number" name="UnitPrice" placeholder="UnitPrice" required>
                        <input type="number" name="Bytes" placeholder="Bytes">
                        <select name="AlbumId">
                        <option disabled selected value> -- select Album -- </option>
                        `

                    albums.forEach(album => {
                        addSection += `
                            <option value="${album.AlbumId}">${album.Title}</option>
                        `
                    })
                    addSection += '</select><select name="MediaTypeId" required> <option disabled selected value> -- select Media Type -- </option>'
                    mediaTypes.forEach(mediaType => {
                        addSection += `
                            <option value="${mediaType.MediaTypeId}">${mediaType.Name}</option>
                        `
                    })
                    addSection += '</select><select name="GenreId"><option disabled selected value> -- select Genre -- </option>'
                    genres.forEach(genre => {
                        addSection += `
                            <option value="${genre.GenreId}">${genre.Name}</option>
                        `
                    })
                    addSection += `</select><button type="button" onclick="action('track', 'POST')">Save shit</button>`
                    $('#add-entry-section').append(addSection)

                }
                break;
            case 'albums':
                console.log('albums')
                const artists = await getSelectorInputs('/api/artists');
                if(artists) {
                    let addSection = `
                        <input type="text" name="Title" placeholder="Title" required>
                        <select name="ArtistId">
                        <option disabled selected value> -- select Artist -- </option>
                        `
                    artists.forEach(artist => {
                        addSection += `
                            <option value="${artist.ArtistId}">${artist.Name}</option>
                        `
                    })
                    addSection += `</select><button type="button" onclick="action('album', 'POST')">Save shit</button>`

                    $('#add-entry-section').append(addSection)
                }
                break;
            case 'artists':
                let addSection = `
                        <input type="text" name="Name" placeholder="Name" required>
                        <button type="button" onclick="action('artist', 'POST')">Save shit</button>
                        `
                $('#add-entry-section').append(addSection)
                break;
        }

    }


    async function paginateResults(baseUrl, page = 1) {
        const response = await getData(baseUrl, page, perPage)
        if (response) {
            $('#item-list').empty();
            pageNumber = page
            if (response.pages) {
                maxPages = response.pages
            }
            $('#page-number').text(`${pageNumber} / ${maxPages}`);
            if (response.tracks) {
                let items = '<tr><th>Name</th><th>Composer</th><th>Milliseconds</th><th>Bytes</th><th>UnitPrice</th><th>Action</th></tr>'
                response.tracks.forEach(track => {
                    items += `
                            <tr id="${track.TrackId}">
                                <td><input class="table-input" name="Name" type="text" value="${track.Name}"></td>
                                <td><input class="table-input" name="Composer" type="text" value="${track.Composer}"></td>
                                <td><input class="table-input" name="Milliseconds" type="text" value="${track.Milliseconds}"></td>
                                <td><input class="table-input" name="Bytes" type="text" value="${track.Bytes}"></td>
                                <td><input class="table-input" name="UnitPrice" type="text" value="${track.UnitPrice}"></td>
                                <td><button class="table-btn btn-edit" onclick="action('track', 'PUT', ${track.TrackId})">Edit</button> <button onclick="action('track', 'DELETE', ${track.TrackId})" class="table-btn btn-delete">delete</button></td>
                            </tr>
                    `

                })
                $('#item-list').append(items);
            } else if (response.artists) {
                let items = '<tr><th>Name</th><th>Action</th></tr>'
                response.artists.forEach(artist => {
                    items += `
                            <tr id="${artist.ArtistId}">
                                <td><input class="table-input" name="Name" type="text" value="${artist.Name}"></td>
                                <td><button class="table-btn btn-edit" onclick="action('artist', 'PUT', ${artist.ArtistId})">Edit</button> <button onclick="action('artist', 'DELETE', ${artist.ArtistId})" class="table-btn btn-delete">delete</button></td>
                            </tr>
                    `
                })
                $('#item-list').append(items);

            } else if (response.albums) {
                let items = '<tr><th>Title</th><th>Action</th></tr>'
                response.albums.forEach(album => {
                    items += `
                            <tr id="${album.AlbumId}">
                                <td><input class="table-input" name="Title" type="text" value="${album.Title}"></td>
                                <td><button class="table-btn btn-edit" onclick="action('album', 'PUT', ${album.AlbumId})">Edit</button> <button onclick="action('album', 'DELETE', ${album.AlbumId})" class="table-btn btn-delete">delete</button></td>
                            </tr>
                    `
                })
                $('#item-list').append(items);
            }
        }
    }

    async function newEntry() {

    }

    async function getData(url = '', page = 1, perPage = 20) {
        const paginateUrl = url + 'page=' + page + '&perPage=' + perPage

        return await fetch(paginateUrl).then(response => response.json())
            .then((tracks) => {
                return tracks
            }).catch(err => {
                return err
            })
    }

    async function action(model, method, id = null) {
        let data = {};
        let url = crudUrl.replace('_model_', model);
        switch (method) {
            case 'DELETE':
                switch (model) {
                    case 'track':
                        data.TrackId = id
                        break;
                    case 'album':
                        data.AlbumId = id
                        break;
                    case 'artist':
                        data.ArtistId = id
                        break;
                }
                break;
            case 'PUT':
                switch (model) {
                    case 'track':
                        data = {
                            TrackId: id,
                            Name: $(`#${id} input[name=Name]`).val(),
                            Composer: $(`#${id} input[name=Composer]`).val(),
                            Milliseconds: parseInt($(`#${id} input[name=Milliseconds]`).val()),
                            Bytes: parseInt($(`#${id} input[name=Bytes]`).val()),
                            UnitPrice: parseFloat($(`#${id} input[name=UnitPrice]`).val())
                        }
                        break;

                    case 'album':
                        data = {
                            AlbumId: id,
                            Title: $(`#${id} input[name=Title]`).val(),
                        }
                        break;
                    case 'artist':
                        data = {
                            ArtistId : id,
                            Name: $(`#${id} input[name=Name]`).val(),
                        }
                        break;
                }
                console.log(data)
                break;
            case 'POST':
                switch (model) {
                    case 'track':
                        url.replace('tracks', 'track')
                        data = {
                            Name: $(`#add-entry-section input[name=Name]`).val(),
                            Composer: $(`#add-entry-section input[name=Composer]`).val(),
                            Milliseconds: parseInt($(`#add-entry-section input[name=Milliseconds]`).val()),
                            Bytes: parseInt($(`#add-entry-section input[name=Bytes]`).val()),
                            UnitPrice: parseFloat($(`#add-entry-section input[name=UnitPrice]`).val()),
                            GenreId: parseInt($(`#add-entry-section select[name=GenreId]`).val()),
                            AlbumId: parseInt($(`#add-entry-section select[name=AlbumId]`).val()),
                            MediaTypeId: parseInt($(`#add-entry-section select[name=MediaTypeId]`).val())
                        }
                        break;
                    case 'album':
                        data = {
                            Title: $(`#add-entry-section input[name=Title]`).val(),
                            ArtistId: parseInt($(`#add-entry-section select[name=ArtistId]`).val())
                        }
                        break;
                    case 'artist':
                        data = {
                            Name: $(`#add-entry-section input[name=Name]`).val(),
                        }
                        break;
                }
                console.log(data)
                break;
        }

        fetch(url, {
            method: method,
            body: JSON.stringify(data)
        }).then(response => response.json()).then(result => {
            if (result && result.errors) {
                $('#action-alert-danger').show();
                $('#action-alert-danger').append(`<small>${JSON.stringify(result.errors)}</small>`)
            } else {
                switch (method) {
                    case 'DELETE':
                        $('#action-alert-success').append(`<small>${model} was deleted</small>`)
                        $(`#${id}`).remove()
                        break;
                    case 'PUT':
                        $('#action-alert-success').append(`<small>${model} was updated</small>`)
                        break;
                    case 'POST':
                        $('#action-alert-success').append(`<small>${model} was created</small>`)
                        break;
                }
                $('#action-alert-success').show();
            }
            setTimeout(() => {
                $('#action-alert-danger').hide()
                $('#action-alert-success').hide()
                $('#action-alert-success').empty()
                $('#action-alert-danger').empty()
                }, 1800);
        }).catch(err => {
            $('#action-alert-danger').show();
            $('#action-alert-danger').append(`<small>An error occured</small>`)
            setTimeout(() => {
                $('#action-alert-danger').hide()
                $('#action-alert-danger').empty()
            }, 1800);
            });
    }
</script>
