<?php


use app\core\ViewHandler;

/** @var  $this ViewHandler */
$this->title = 'Browse'
?>
<div class="container">
    <section class="browse-menu">
        <button id="btn-tracks">Tracks</button>
        <button id="btn-albums">Albums</button>
        <button id="btn-artists">Artists</button>
    </section>

    <section class="browse-section">
        <div class="cards" id="cards">

        </div>

        <div class="pagination">
            <button class="float-left" id="btn-prev">&laquo;</button>
            <p id="page-number">1</p>
            <button class="float-right" id="btn-next">&raquo;</button>
        </div>
    </section>
</div>



<script>

    let activeSection = 'tracks'
    const tracksUrl = '/api/tracks?withRelations=true&';
    const albumsUrl = '/api/albums?withRelations=true&';
    const artistsUrl = '/api/artists?';
    const addToCartUrl = '/api/cart'
    const getCartUrl = '/api/cart'

    const perPage = 8;
    let pageNumber = 1;
    let maxPages = 1;


    $(`#btn-${activeSection}`).addClass('is-active')

    // This is an efficient way to referer to a variable in the scope by string
    paginateResults(eval(`${activeSection}Url`))

    $('#btn-next').click(function () {
        if (pageNumber < maxPages) {
            paginateResults(eval(`${activeSection}Url`), pageNumber +=1)
        }
    });

    $('#btn-prev').click(function () {
        if (pageNumber > 1) {
            paginateResults(eval(`${activeSection}Url`), pageNumber -=1)
        }
    });

    $('#btn-tracks').click(function () {
        $(`#btn-${activeSection}`).removeClass('is-active')
        activeSection = 'tracks'
        $(`#btn-${activeSection}`).addClass('is-active')
        paginateResults(eval(`${activeSection}Url`))
    });

    $('#btn-albums').click(function () {
        $(`#btn-${activeSection}`).removeClass('is-active')
        activeSection = 'albums'
        $(`#btn-${activeSection}`).addClass('is-active')
        paginateResults(eval(`${activeSection}Url`))
    });

    $('#btn-artists').click(function () {
        $(`#btn-${activeSection}`).removeClass('is-active')
        activeSection = 'artists'
        $(`#btn-${activeSection}`).addClass('is-active')
        paginateResults(eval(`${activeSection}Url`))
    });


    async function paginateResults(baseUrl, page = 1) {
        const cart = await fetch(getCartUrl)
            .then(response => response.json())
            .then(data => {return data});
        const response = await getData(baseUrl, page, perPage)
        if (response) {
            $('#cards').empty();
            $('#cards').empty();
            $('#cards').empty();
            pageNumber = page
            if (response.tracks) {
                if (response.pages) {
                    maxPages = response.pages
                }
                $('#page-number').text(`${pageNumber} / ${maxPages}`);
                let items = ''
                response.tracks.forEach(track => {
                    let isInCart = false;
                    if (cart && cart.cart) {

                        Object.keys(cart.cart).forEach(key => {
                            if (cart.cart[key].TrackId === track.TrackId) {
                                isInCart = true
                            }
                        });
                    }

                    items += `
                            <a href="#" class="card card-track ${isInCart ? 'added-to-cart' : ''}" id="${track.TrackId}" onclick="addToCart(${track.TrackId})">
                                <div class="inner">
                                    <h2 class="title">${track.Name}</h2>
                                    <p><small>Album:</small> ${track.Album.Title}</p>
                                    <p><small>genre:</small> ${track.Genre.Name}</p>
                                    <time class="subtitle">${Number(track.Milliseconds / 1000 / 60).toFixed(2)} min<time>
                                </div>
                            </a>`

                })
                $('#cards').append(items);
            } else if (response.artists) {
                if (response.pages) {
                    maxPages = response.pages
                }
                $('#page-number').text(`${pageNumber} / ${maxPages}`);
                let items = ''
                response.artists.forEach(artist => {
                    //console.log(track)
                    items += `
                    <a href="#" class="card card-artist" id="art-${artist.ArtistId}">
                        <div class="inner">
                            <h2 class="title">${artist.Name}</h2>

                        </div>
                    </a>`
                })
                $('#cards').append(items);

            } else if (response.albums) {
                if (response.pages) {
                    maxPages = response.pages
                }
                $('#page-number').text(`${pageNumber} / ${maxPages}`);
                let items = ''
                response.albums.forEach(album => {
                    //console.log(track)
                    items += `
                    <a href="#" class="card card-album" id="alb-${album.AlbumId}">
                        <div class="inner">
                            <h2 class="title">${album.Title}</h2>
                            <p><small>Artist:</small> ${album.Artist.Name}</p>
                        </div>
                    </a>`
                })
                $('#cards').append(items);
            }
        }

        //console.log(test)
    }

    async function addToCart(trackId) {
        if (trackId) {
            return await fetch(addToCartUrl, {
                method: 'PUT',
                body: JSON.stringify({trackId : trackId})
            }).then(response => response.json())
                .then((cart) => {
                    if (cart && cart.track) {
                        $(`#${trackId}`).addClass('added-to-cart')
                    }
                    return cart
                }).catch(err => {
                    return err
                })
        }

    }

    async function getData(url = '', page = 1, perPage = 20) {
        const paginateUrl = url + 'page=' + page + '&perPage=' + perPage

        return await fetch(paginateUrl).then(response => response.json())
            .then((tracks) => {
                return tracks
            }).catch(err => {
                return err
            })
    }
</script>
